#pragma checksum "C:\Users\Marcus\Documents\Visual Studio 2017\Projects\Centroserv\Centroserv\Centroserv\Views\Agenda\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "0539947fec20a13be0ca33a481301d0648171d94"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Agenda_Index), @"mvc.1.0.view", @"/Views/Agenda/Index.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Agenda/Index.cshtml", typeof(AspNetCore.Views_Agenda_Index))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 2 "C:\Users\Marcus\Documents\Visual Studio 2017\Projects\Centroserv\Centroserv\Centroserv\Views\Agenda\Index.cshtml"
using Centroserv.ViewModels;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"0539947fec20a13be0ca33a481301d0648171d94", @"/Views/Agenda/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"44e4996073d576984abf173902213fa20e43f99f", @"/Views/_ViewImports.cshtml")]
    public class Views_Agenda_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<Centroserv.ViewModels.AgendaIndexViewModel>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("style", new global::Microsoft.AspNetCore.Html.HtmlString("color:white"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "index", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-controller", "entregas", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Edit", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_4 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-controller", "Entregas", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_5 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("name", "_EntregaIncluir", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_6 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("name", "_ServicoIncluir", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_7 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/libs/vanillaCalendar.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.PartialTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_PartialTagHelper;
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(0, 2, true);
            WriteLiteral("\r\n");
            EndContext();
            BeginContext(84, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 5 "C:\Users\Marcus\Documents\Visual Studio 2017\Projects\Centroserv\Centroserv\Centroserv\Views\Agenda\Index.cshtml"
  
    Layout = "~/Views/Shared/_Layout_main.cshtml";
    ViewBag.Title = "Agenda";

#line default
#line hidden
            BeginContext(176, 4, true);
            WriteLiteral("\r\n\r\n");
            EndContext();
            BeginContext(234, 336, true);
            WriteLiteral(@"<style>
    input[type=date]:required:invalid::-webkit-datetime-edit {
        color: transparent;
    }

    input[type=date]:focus::-webkit-datetime-edit {
        color: black !important;
    }
</style>


<h2 id=""TituloTarefas"" style=""padding:5px;"" class=""text-secondary text-center"">Agenda do Dia <span class=""text-info"">");
            EndContext();
            BeginContext(571, 12, false);
#line 23 "C:\Users\Marcus\Documents\Visual Studio 2017\Projects\Centroserv\Centroserv\Centroserv\Views\Agenda\Index.cshtml"
                                                                                                                Write(ViewBag.data);

#line default
#line hidden
            EndContext();
            BeginContext(583, 1883, true);
            WriteLiteral(@"</span></h2>

<div class=""row "">
    <div class=""col-lg-3"">
        <div class=""container"">
            <div id=""v-cal"">
                <div class=""vcal-header"">
                    <button class=""vcal-btn"" data-calendar-toggle=""previous"">
                        <svg height=""24"" version=""1.1"" viewbox=""0 0 24 24"" width=""24"" xmlns=""http://www.w3.org/2000/svg"">
                            <path d=""M20,11V13H8L13.5,18.5L12.08,19.92L4.16,12L12.08,4.08L13.5,5.5L8,11H20Z""></path>
                        </svg>
                    </button>

                    <div class=""vcal-header__label"" data-calendar-label=""month"">
                        March 2017
                    </div>


                    <button class=""vcal-btn"" data-calendar-toggle=""next"">
                        <svg height=""24"" version=""1.1"" viewbox=""0 0 24 24"" width=""24"" xmlns=""http://www.w3.org/2000/svg"">
                            <path d=""M4,11V13H16L10.5,18.5L11.92,19.92L19.84,12L11.92,4.08L10.5,5.5L16,11H4Z""></path>
  ");
            WriteLiteral(@"                      </svg>
                    </button>
                </div>
                <div class=""vcal-week"">
                    <span>Seg</span>
                    <span>Ter</span>
                    <span>Qua</span>
                    <span>Qui</span>
                    <span>Sex</span>
                    <span>Sab</span>
                    <span>Dom</span>
                </div>
                <div class=""vcal-body"" data-calendar-area=""month""></div>
            </div>


        </div>
    </div>
    <div class=""col-lg-4 center-align"" style=""padding-top:20px;margin-right:10px"">
        <div class=""card"">
            <div class=""card-header lead text-center text-light"" style=""background-color:cadetblue"">
                <div class=""row "">

                    <div class=""col-lg"" style=""font-size:1.1em;"">");
            EndContext();
            BeginContext(2466, 80, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "729ac80ced744641ad937f721b08b9c7", async() => {
                BeginContext(2534, 8, true);
                WriteLiteral("Entregas");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Controller = (string)__tagHelperAttribute_2.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_2);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(2546, 8, true);
            WriteLiteral("</div>\r\n");
            EndContext();
            BeginContext(2711, 320, true);
            WriteLiteral(@"                </div>
            </div>
            <div class=""card-body"" id=""divEntregas"">
                <div class=""text-right"">
                    <button type=""button"" class=""btn btn-outline-primary btn-sm"" data-toggle=""modal"" data-target=""#ModalMaterial"">Agendar Entrega</button>
                </div>
");
            EndContext();
#line 77 "C:\Users\Marcus\Documents\Visual Studio 2017\Projects\Centroserv\Centroserv\Centroserv\Views\Agenda\Index.cshtml"
                  
                    foreach (var x in Model.Entregas)
                    {
                        

#line default
#line hidden
            BeginContext(3180, 24, true);
            WriteLiteral("                        ");
            EndContext();
            BeginContext(3204, 416, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "0e6907939aeb4aa5888c3268a27e76bb", async() => {
                BeginContext(3279, 33, true);
                WriteLiteral("\r\n                            <h5");
                EndContext();
                BeginWriteAttribute("class", " class=\"", 3312, "\"", 3412, 1);
#line 83 "C:\Users\Marcus\Documents\Visual Studio 2017\Projects\Centroserv\Centroserv\Centroserv\Views\Agenda\Index.cshtml"
WriteAttributeValue("", 3320, x.DataEntrega < DateTime.Now.Date && x.Entregue == false ? "text-danger" : "text-success", 3320, 92, false);

#line default
#line hidden
                EndWriteAttribute();
                BeginWriteAttribute("style", " style=\"", 3413, "\"", 3503, 1);
#line 83 "C:\Users\Marcus\Documents\Visual Studio 2017\Projects\Centroserv\Centroserv\Centroserv\Views\Agenda\Index.cshtml"
WriteAttributeValue("", 3421, x.Entregue == true ? "text-decoration: line-through" : "text-decoration: none" , 3421, 82, false);

#line default
#line hidden
                EndWriteAttribute();
                BeginContext(3504, 35, true);
                WriteLiteral(">\r\n                                ");
                EndContext();
                BeginContext(3540, 15, false);
#line 84 "C:\Users\Marcus\Documents\Visual Studio 2017\Projects\Centroserv\Centroserv\Centroserv\Views\Agenda\Index.cshtml"
                           Write(x.Cliente.Alias);

#line default
#line hidden
                EndContext();
                BeginContext(3555, 61, true);
                WriteLiteral("\r\n                            </h5>\r\n                        ");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_3.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_3);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Controller = (string)__tagHelperAttribute_4.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_4);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-id", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#line 82 "C:\Users\Marcus\Documents\Visual Studio 2017\Projects\Centroserv\Centroserv\Centroserv\Views\Agenda\Index.cshtml"
                                                                         WriteLiteral(x.IdEntrega);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(3620, 135, true);
            WriteLiteral("\r\n                        <table class=\"table table-sm table-bordered\" style=\"font-size:0.8rem\">\r\n                            <tbody>\r\n");
            EndContext();
#line 89 "C:\Users\Marcus\Documents\Visual Studio 2017\Projects\Centroserv\Centroserv\Centroserv\Views\Agenda\Index.cshtml"
                                  
                                    foreach (var i in x.Itens)
                                    {

#line default
#line hidden
            BeginContext(3894, 43, true);
            WriteLiteral("                                        <tr");
            EndContext();
            BeginWriteAttribute("style", " style=\"", 3937, "\"", 4057, 1);
#line 92 "C:\Users\Marcus\Documents\Visual Studio 2017\Projects\Centroserv\Centroserv\Centroserv\Views\Agenda\Index.cshtml"
WriteAttributeValue("", 3945, x.Entregue == true ? "text-decoration: line-through;color:lightgrey" : "text-decoration: none; colo:#212529" , 3945, 112, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(4058, 51, true);
            WriteLiteral(">\r\n                                            <td>");
            EndContext();
            BeginContext(4110, 20, false);
#line 93 "C:\Users\Marcus\Documents\Visual Studio 2017\Projects\Centroserv\Centroserv\Centroserv\Views\Agenda\Index.cshtml"
                                           Write(i.Material.Descricao);

#line default
#line hidden
            EndContext();
            BeginContext(4130, 55, true);
            WriteLiteral("</td>\r\n                                            <td>");
            EndContext();
            BeginContext(4186, 12, false);
#line 94 "C:\Users\Marcus\Documents\Visual Studio 2017\Projects\Centroserv\Centroserv\Centroserv\Views\Agenda\Index.cshtml"
                                           Write(i.Quantidade);

#line default
#line hidden
            EndContext();
            BeginContext(4198, 1, true);
            WriteLiteral(" ");
            EndContext();
            BeginContext(4200, 18, false);
#line 94 "C:\Users\Marcus\Documents\Visual Studio 2017\Projects\Centroserv\Centroserv\Centroserv\Views\Agenda\Index.cshtml"
                                                         Write(i.Material.Unidade);

#line default
#line hidden
            EndContext();
            BeginContext(4218, 54, true);
            WriteLiteral("</td>\r\n                                        </tr>\r\n");
            EndContext();
#line 96 "C:\Users\Marcus\Documents\Visual Studio 2017\Projects\Centroserv\Centroserv\Centroserv\Views\Agenda\Index.cshtml"

                                    }
                                

#line default
#line hidden
            BeginContext(4348, 106, true);
            WriteLiteral("\r\n                            </tbody>\r\n                        </table>\r\n                        <br />\r\n");
            EndContext();
#line 103 "C:\Users\Marcus\Documents\Visual Studio 2017\Projects\Centroserv\Centroserv\Centroserv\Views\Agenda\Index.cshtml"
                                    }
                

#line default
#line hidden
            BeginContext(4512, 644, true);
            WriteLiteral(@"
            </div>
        </div>
        <br />
    </div>
    <div class=""col-lg-4"" style=""padding-top:20px;"">
        <div class=""card"">
            <div class=""card-header lead text-center text-light"" style=""background-color:cadetblue"">
                <div class=""row"">
                    <div class=""col-lg"">Serviços</div>
                </div>
            </div>
            <div class=""card-body "">
                <div class=""text-right"">
                    <button type=""button"" class=""btn btn-outline-primary btn-sm"" data-toggle=""modal"" data-target=""#ModalServico"">Agendar Serviço</button>
                </div>
");
            EndContext();
#line 121 "C:\Users\Marcus\Documents\Visual Studio 2017\Projects\Centroserv\Centroserv\Centroserv\Views\Agenda\Index.cshtml"
                  
                    foreach (var r in Model.Servicos)
                    {

#line default
#line hidden
            BeginContext(5254, 118, true);
            WriteLiteral("                        <div class=\"row\" style=\"padding-top:10px\">\r\n                            <div class=\"col-lg-5\">");
            EndContext();
            BeginContext(5373, 15, false);
#line 125 "C:\Users\Marcus\Documents\Visual Studio 2017\Projects\Centroserv\Centroserv\Centroserv\Views\Agenda\Index.cshtml"
                                             Write(r.Cliente.Alias);

#line default
#line hidden
            EndContext();
            BeginContext(5388, 59, true);
            WriteLiteral(" </div>\r\n                            <div class=\"col-lg-4\">");
            EndContext();
            BeginContext(5448, 13, false);
#line 126 "C:\Users\Marcus\Documents\Visual Studio 2017\Projects\Centroserv\Centroserv\Centroserv\Views\Agenda\Index.cshtml"
                                             Write(r.TipoServico);

#line default
#line hidden
            EndContext();
            BeginContext(5461, 69, true);
            WriteLiteral("</div>\r\n                            <div class=\"col-lg-3 text-right\">");
            EndContext();
            BeginContext(5531, 40, false);
#line 127 "C:\Users\Marcus\Documents\Visual Studio 2017\Projects\Centroserv\Centroserv\Centroserv\Views\Agenda\Index.cshtml"
                                                        Write(r.HoraServico.ToString().Substring(0, 5));

#line default
#line hidden
            EndContext();
            BeginContext(5571, 329, true);
            WriteLiteral(@"</div>
                            <div class=""col-lg-1""></div>
                        </div>
                        <div class=""row border-bottom"">
                            <div class=""col-lg-12 text-left "" style=""padding-bottom:10px"">
                                <span style=""font-size:0.8rem;"" class=""text-info"">");
            EndContext();
            BeginContext(5901, 5, false);
#line 132 "C:\Users\Marcus\Documents\Visual Studio 2017\Projects\Centroserv\Centroserv\Centroserv\Views\Agenda\Index.cshtml"
                                                                             Write(r.Obs);

#line default
#line hidden
            EndContext();
            BeginContext(5906, 77, true);
            WriteLiteral("</span>\r\n                            </div>\r\n                        </div>\r\n");
            EndContext();
#line 135 "C:\Users\Marcus\Documents\Visual Studio 2017\Projects\Centroserv\Centroserv\Centroserv\Views\Agenda\Index.cshtml"

                    }
                

#line default
#line hidden
            BeginContext(6027, 230, true);
            WriteLiteral("\r\n\r\n\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n\r\n\r\n<!-- Modal Material-->\r\n<div class=\"modal fade\" id=\"ModalMaterial\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"TituloModalCentralizado\" aria-hidden=\"true\">\r\n    ");
            EndContext();
            BeginContext(6257, 34, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("partial", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "4e99242a5deb46f8b8efd58c3fb7d84a", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_PartialTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.PartialTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_PartialTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_PartialTagHelper.Name = (string)__tagHelperAttribute_5.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_5);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(6291, 168, true);
            WriteLiteral("\r\n</div>\r\n\r\n<!-- Modal Servico -->\r\n<div class=\"modal fade\" id=\"ModalServico\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"TituloModalServico\" aria-hidden=\"true\">\r\n    ");
            EndContext();
            BeginContext(6459, 56, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("partial", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "0b43aa21815444509e201e3db52822c3", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_PartialTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.PartialTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_PartialTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_PartialTagHelper.Name = (string)__tagHelperAttribute_6.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_6);
#line 155 "C:\Users\Marcus\Documents\Visual Studio 2017\Projects\Centroserv\Centroserv\Centroserv\Views\Agenda\Index.cshtml"
__Microsoft_AspNetCore_Mvc_TagHelpers_PartialTagHelper.Model = Model.Servico;

#line default
#line hidden
            __tagHelperExecutionContext.AddTagHelperAttribute("model", __Microsoft_AspNetCore_Mvc_TagHelpers_PartialTagHelper.Model, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(6515, 1182, true);
            WriteLiteral(@"
</div>


<style>
    /** {
        box-sizing: border-box;

    }

    .autocomplete {
        the container must be positioned relative:
        position: relative;
        display: inline-block;
    }


    .autocomplete-items {
        position: absolute;
        border: 1px solid #d4d4d4;
        border-bottom: none;
        border-top: none;
        z-index: 99;
        position the autocomplete items to be the same width as the container:
        top: 100%;
        left: 0;
        right: 0;
    }

        .autocomplete-items div {
                                               padding: 10px;
                                               cursor: pointer;
                                               background-color: #fff;
            border-bottom: 1px solid #d4d4d4;
        }

            .autocomplete-items div:hover {
                when hovering an item:
                background-color: #e9e9e9;
            }

    .autocomplete-active {
        when na");
            WriteLiteral("vigating through the items using the arrow keys:\r\n        background-color: DodgerBlue !important;\r\n        color: #ffffff;\r\n    }*/\r\n</style>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n");
            EndContext();
            BeginContext(7697, 49, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "88ca41c31d3c4a45a8cd05122f837b99", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_7);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(7746, 182, true);
            WriteLiteral("\r\n\r\n\r\n<script>\r\n    window.addEventListener(\'load\', function () {\r\n        vanillaCalendar.init({\r\n            disablePastDays: false\r\n        });\r\n    });\r\n    \r\n\r\n</script>\r\n\r\n\r\n\r\n");
            EndContext();
            BeginContext(28434, 2, true);
            WriteLiteral("\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<Centroserv.ViewModels.AgendaIndexViewModel> Html { get; private set; }
    }
}
#pragma warning restore 1591
