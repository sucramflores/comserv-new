﻿using Centroserv.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Centroserv.Services
{
    public interface IServicos
    {
        Servicos Add(Servicos servico);
        void Remove(int idServico);
        void Update(Servicos servico);
        List<Servicos> GetAll(DateTime Dt);
        Servicos Get(int idServico);
    }



    public class InMemoryServicos : IServicos
    {
        public List<Servicos> _servicos { get; set; }
        public InMemoryServicos()
        {
            _servicos = new List<Servicos>
            {
                new Servicos{ DataServico = new DateTime(2019,7,30), HoraServico = new TimeSpan(12,30,00), IdCliente = 12, IdServico = 1, TipoServico = Models.ValoresPadroes.TiposServico.Lavagem, Obs = "Esperar saida de todos os carros "},
                new Servicos{ DataServico = new DateTime(2019,7,30), HoraServico = new TimeSpan(08,30,00), IdCliente = 9, IdServico = 2, TipoServico = Models.ValoresPadroes.TiposServico.Gramado, Obs = "Chegar antes das 7am para pegar chave"},
                new Servicos{ DataServico = DateTime.Now.Date, HoraServico = new TimeSpan(8,30,00), IdCliente = 8, IdServico = 3, TipoServico = Models.ValoresPadroes.TiposServico.Avaliacao, Obs = "Morador reclamou para sindico"},
                new Servicos{ DataServico = DateTime.Now.Date, HoraServico = new TimeSpan(10,00,00), IdCliente = 9, IdServico = 4, TipoServico = Models.ValoresPadroes.TiposServico.Gramado, Obs = "Cuidar as flores"},
                new Servicos{ DataServico = DateTime.Now.Date, HoraServico = new TimeSpan(14,30,00), IdCliente = 22, IdServico = 5, TipoServico = Models.ValoresPadroes.TiposServico.Gramado, Obs = "A pedido do Sydney (Banco de Imovesi)"}
            };
        }

        public Servicos Add(Servicos servico)
        {
            int newId = _servicos.Max(e => e.IdServico) + 1;
            servico.IdServico = newId;
            _servicos.Add(servico);
            return servico;
        }

        public Servicos Get(int idServico)
        {
            return _servicos.FirstOrDefault(e => e.IdServico == idServico);
        }

        public List<Servicos> GetAll(DateTime dataServico)
        {
            return _servicos.Where(e=>e.DataServico == dataServico).ToList();
        }

        public void Remove(int idServico)
        {
            _servicos = _servicos.Where(e => e.IdServico != idServico).ToList();
        }

        public void Update(Servicos servico)
        {
            foreach(var x in _servicos.Where(e => e.IdServico == servico.IdServico))
            {
                x.DataServico = servico.DataServico;
                x.IdCliente = servico.IdCliente;
                x.Obs = servico.Obs;
                x.TipoServico = servico.TipoServico;
                x.HoraServico = servico.HoraServico;
            }
        }
    }
}
