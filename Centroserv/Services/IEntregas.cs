﻿using Centroserv.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using System.Data;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;

namespace Centroserv.Services
{
    public interface IEntregas
    {
        IEnumerable<Entregas> GetAll(DateTime data, bool showAll);
        Entregas Get(int id);
        Entregas Add(Entregas entrega);
        void Remove(int idEntrega);
        void Update(Entregas entrega);


        // Itens (material) que sera Entregue
        List<EntregaItens> GetAllItems(int idEntrega);
        EntregaItens GetItem(int idEntrega, int idEntregaItem);
        EntregaItens AddItem(int idEntrega, EntregaItens item);
        void RemoveItem(int idEntrega, int idEntregaItem);
        void RemoveAllItens(int idEntrega);

    }



    public class InMemoryEntregas : IEntregas
    {
        private List<Entregas> _entregas;
        private List<EntregaItens> _entregaItens;
        private IMaterial _materiais;

        public InMemoryEntregas(IMaterial material)
        {
            _materiais = material;
            _entregaItens = new List<EntregaItens>
            {
                new EntregaItens{  IdEntrega = 1, IdItem = 1, Quantidade = 2, CustoFinal = 1221.98m, DataEntrega = DateTime.Now.Date},
                new EntregaItens{  IdEntrega = 1, IdItem = 2, Quantidade = 3, CustoFinal = 21.98m, DataEntrega = DateTime.Now.Date},
                new EntregaItens{  IdEntrega = 1, IdItem = 3, Quantidade = 12, CustoFinal = (decimal)32, DataEntrega = DateTime.Now.Date},
                new EntregaItens{  IdEntrega = 2, IdItem = 4, Quantidade = 1, CustoFinal = (decimal)29.18, DataEntrega = DateTime.Now.Date},
                new EntregaItens{  IdEntrega = 3, IdItem = 2, Quantidade = (decimal)2.5, CustoFinal = (decimal)41.98, DataEntrega = DateTime.Now.Date},
                new EntregaItens{  IdEntrega = 3, IdItem = 4, Quantidade = 7, CustoFinal = (decimal)201.67, DataEntrega = DateTime.Now.Date},
                new EntregaItens{  IdEntrega = 4, IdItem = 1, Quantidade = 7, CustoFinal = (decimal)201.67, DataEntrega = DateTime.Now.Date}

            };

            _entregas = new List<Entregas>
            {
                new Entregas{ IdCliente = 12, IdEntrega = 1, CustoTotal = 31.41m, Obs= "Estregue conforme solicitado", DataEntrega = DateTime.Now.Date.AddDays(-4), Solicitado = DateTime.Now.Date, Itens = new List<EntregaItens>(), Entregue = false },
                new Entregas{ IdCliente = 10, IdEntrega = 2, CustoTotal = 1231.51m, Obs= "Maria pediu", DataEntrega = DateTime.Now.Date, Solicitado = DateTime.Now.Date, Itens = new List<EntregaItens>(), Entregue = true},
                new Entregas{ IdCliente = 11, IdEntrega = 3, CustoTotal = 431.61m, Obs= "Funcionario saiu antes de se entregue", DataEntrega = DateTime.Now.Date.AddDays(-3), Solicitado = DateTime.Now.Date, Itens = new List<EntregaItens>(), Entregue = true},
                new Entregas{ IdCliente = 22, IdEntrega = 4, CustoTotal = 2345.01m, Obs= "Pedido cancelado pelo sindico", DataEntrega = DateTime.Now.Date, Solicitado = DateTime.Now.Date, Itens = new List<EntregaItens>(), Entregue=false}

            };

        }

        public Entregas Add(Entregas entrega)
        {
            entrega.IdEntrega = _entregas.Max(e => e.IdEntrega) + 1;
            _entregas.Add(entrega);
            return entrega;

        }

        public EntregaItens AddItem(int idEntrega, EntregaItens item)
        {
            EntregaItens model1 = new EntregaItens();
            var oMaterial = _materiais.Get(item.IdItem);
            model1.CustoFinal = item.Quantidade * (decimal)oMaterial.ValorUnitario;
            model1.DataEntrega = item.DataEntrega;
            model1.IdEntrega = idEntrega;
            model1.IdItem = item.IdItem;
            model1.Quantidade = item.Quantidade;
            _entregaItens.Add(model1);

            
            return item;

        }

        public Entregas Get(int id)
        {
            var model = _entregas.FirstOrDefault(e => e.IdEntrega == id);
            model.Itens = _entregaItens.Where(e => e.IdEntrega == id).ToList();
            return model;
        }



        public List<EntregaItens> GetAllItems(int idEntrega)
        {
            List<EntregaItens> model = _entregaItens.Where(e => e.IdEntrega == idEntrega).ToList();
            return model;
        }

        public EntregaItens GetItem(int idEntrega, int idEntregaItem)
        {
            return _entregaItens.FirstOrDefault(e => e.IdEntrega == idEntrega && e.IdItem == idEntregaItem);
        }

        public void Remove(int idEntrega)
        {
            _entregas = _entregas.Where(e => e.IdEntrega != idEntrega).ToList();
        }

        public void RemoveItem(int idEntrega, int idEntregaItem)
        {
            throw new NotImplementedException();
        }

        public void Update(Entregas entrega)
        {
            foreach (var x in _entregas.Where(e=>e.IdEntrega == entrega.IdEntrega)){
                x.Obs = entrega.Obs;
                x.Responsavel = entrega.Responsavel;
                x.Entregue = entrega.Entregue;
                x.DataEntrega = entrega.DataEntrega;
            }
        }

        public void RemoveAllItens(int idEntrega)
        {
            _entregaItens = _entregaItens.Where(e => e.IdEntrega != idEntrega).ToList();
        }

        public IEnumerable<Entregas> GetAll(DateTime data, bool showAll)
        {
            if (showAll)
            {
                List<Entregas> model = _entregas.ToList();
                foreach (Entregas x in model)
                {
                    x.Itens = GetAllItems(x.IdEntrega);
                }
                return model;
            }
            else
            {
                List<Entregas> model = new List<Entregas>();
                if (data == null)
                {
                    model = _entregas.Where(e => e.DataEntrega == DateTime.Now.Date || (e.DataEntrega < DateTime.Now.Date && e.Entregue == false)).ToList();
                }
                else if(data>DateTime.Now.Date)
                {
                    model = _entregas.Where(e => e.DataEntrega == data).ToList();
                }
                else
                {
                    model = _entregas.Where(e => e.DataEntrega == data || (e.DataEntrega <= data && e.Entregue != true)).ToList();
                }

                foreach (var x in model)
                {
                    x.Itens = GetAllItems(x.IdEntrega);
                }
                return model;
            }

        }
    }





    // Database Implementation


    public class DBEntregas : IEntregas
    {
        private string _connectionString;
        private IDbConnection db;

        public DBEntregas(IConfiguration config)
        {
            _connectionString = config.GetConnectionString("DefaultConnection");
            db = new SqlConnection(_connectionString);
        }

        public Entregas Add(Entregas entrega)
        {
            string sql = "insert into entregas(idEntrega,idcliente,solicitado,custotal,obs,reclamacao) values(@idEntrega,@idcliente,@solicitado,@custotal,@obs,@reclamacao)";
            var id = db.Query<int>(sql, entrega).Single();
            entrega.IdEntrega = id;
            return entrega;
        }

        public EntregaItens AddItem(int idEntrega, EntregaItens item)
        {
            throw new NotImplementedException();
        }

        public Entregas Get(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Entregas> GetAll(DateTime data, bool showAll)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<EntregaItens> GetAllItems(int idEntrega)
        {
            throw new NotImplementedException();
        }

        public EntregaItens GetItem(int idEntrega, int idEntregaItem)
        {
            throw new NotImplementedException();
        }

        public void Remove(int idEntrega)
        {
            throw new NotImplementedException();
        }

        public void RemoveItem(int idEntrega, int idEntregaItem)
        {
            throw new NotImplementedException();
        }

       

        public void RemoveAllItens(int idEntrega, EntregaItens item)
        {
            throw new NotImplementedException();
        }

        List<EntregaItens> IEntregas.GetAllItems(int idEntrega)
        {
            throw new NotImplementedException();
        }

        public void RemoveAllItens(int idEntrega)
        {
            throw new NotImplementedException();
        }

        public void Update(Entregas entrega)
        {
            throw new NotImplementedException();
        }
    }

}
