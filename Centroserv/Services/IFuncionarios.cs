﻿using Centroserv.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Centroserv.Models.ValoresPadroes;


namespace Centroserv.Services
{
    public interface IFuncionarios
    {
        IEnumerable<Funcionarios> GetAll();
        Funcionarios Get(int id);
        Funcionarios Add(Funcionarios funcionario);
        void Update(Funcionarios funcionario);
        void Delete(int id);

    }


    public class InMemoryFuncionarios : IFuncionarios
    {
        private List<Funcionarios> _funcionarios;

        public InMemoryFuncionarios()
        {
            _funcionarios = new List<Funcionarios>
            {
                new Funcionarios { Admissao = DateTime.Now.ToString("dd/MM/yyyy"), Alias = "Maria Eduarda", Bairro="Rosario", Cargo=CargoFuncionario.Limpeza, Celular ="(55)9715-8789", Fone = "(55)3226-4564",
                    Cidade ="Santa Maria", ContatoEmergencia = "Pedro (Vizinho)", ContatoEmergenciaCelular = "(51)4552-4587", Endereco = "Rua Macieira, 456", Id = 1,
                    IndicadaPor = "Rita Pereira", Nascimento = DateTime.Now.ToString("dd/MM/yyyy"), Nome = "Mariua Eduarda da Cunha", Obs = "Deixou curriculo", Status = StatusFuncionario.Contratado, UF = "RS", Foto = "/pictures/person1.jpg"},

                new Funcionarios { Admissao = DateTime.Now.ToString("dd/MM/yyyy"), Alias = "Joao", Bairro="Centro", Cargo=CargoFuncionario.Porteiro, Celular ="(55)9878-8789",Fone = "(55)3026-4564",
                    Cidade ="Santa Maria", ContatoEmergencia = "Maria (Cunhada)", ContatoEmergenciaCelular = "(51)4552-4587", Endereco = "SQ2 Q3 C 5", Id = 2,
                    IndicadaPor = "SINE", Nascimento = DateTime.Now.ToString("dd/MM/yyyy"), Nome = "Joao Rodrigues Poleo", Obs = "Faltou primeiro dia", Status = StatusFuncionario.Contratado, UF = "RS",Foto = "/pictures/person2.jpg"},

                new Funcionarios { Admissao = DateTime.Now.ToString("dd/MM/yyyy"), Alias = "Daiane ", Bairro="Parque Pinheiro", Cargo=CargoFuncionario.Limpeza, Celular ="(51)8987-8111",Fone = "(55)3126-8564",
                    Cidade ="Santa Maria", ContatoEmergencia = "Susana (Mae)", ContatoEmergenciaCelular = "(55)3056-4587", Endereco = "Rua Apple Dr, 456", Id = 3,
                    IndicadaPor = "Carlos (Imobiliaria Cancian)", Nascimento = DateTime.Now.ToString("dd/MM/yyyy"), Nome = "Daiane da Silva Barbosa", Obs = "Pediu Feria en Dez/2020", Status = StatusFuncionario.Ferias, UF = "RS",Foto = "/pictures/person3.jpg"},

                new Funcionarios { Admissao = DateTime.Now.ToString("dd/MM/yyyy"), Alias = "Eliane", Bairro="Santa Marta", Cargo=CargoFuncionario.Administrativo, Celular ="(55)4789-7895",Fone = "(55)3726-1064",
                    Cidade ="Santa Maria", ContatoEmergencia = "Cecer (Bispo)", ContatoEmergenciaCelular = "(51)4552-4222", Endereco = "Rua Andre Marques, 456", Id = 4,
                    Nascimento = DateTime.Now.ToString("dd/MM/yyyy"), Nome = "Eliane Saboia Peixoto", Obs = "Conseguiu outro trabalho", Status = StatusFuncionario.Desligado, UF = "RS",Foto = "/pictures/person4.jpg"}

            };
        }

        public Funcionarios Add(Funcionarios funcionario)
        {
            if (_funcionarios.Count > 0)
            {
                funcionario.Id = _funcionarios.Max(e => e.Id) + 1;
            }
            else
            {
                funcionario.Id = 1;
            }
            _funcionarios.Add(funcionario);
            return funcionario;
        }

        public void Delete(int id)
        {
            _funcionarios = _funcionarios.Where(e => e.Id != id).ToList();
        }

        public Funcionarios Get(int id)
        {
            return _funcionarios.FirstOrDefault(e => e.Id == id);
        }

        public IEnumerable<Funcionarios> GetAll()
        {
            return _funcionarios.OrderBy(e => e.Nome);
        }

        public void Update(Funcionarios funcionario)
        {
            foreach (var x in _funcionarios.Where(e => e.Id == funcionario.Id))
            {
                x.Admissao = funcionario.Admissao;
                x.Alias = funcionario.Alias;
                x.Bairro = funcionario.Bairro;
                x.Cargo = funcionario.Cargo;
                x.Celular = funcionario.Celular;
                x.Cidade = funcionario.Cidade;
                x.ContatoEmergencia = funcionario.ContatoEmergencia;
                x.ContatoEmergenciaCelular = funcionario.ContatoEmergenciaCelular;
                x.Domingo = funcionario.Domingo;
                x.Endereco = funcionario.Endereco;
                x.Fone = funcionario.Fone;
                x.IndicadaPor = funcionario.IndicadaPor;
                x.Nascimento = funcionario.Nascimento;
                x.Nome = funcionario.Nome;
                x.Obs = funcionario.Obs;
                x.Quarta = funcionario.Quarta;
                x.Quinta = funcionario.Quinta;
                x.Sexta = funcionario.Sexta;
                x.Sabado = funcionario.Sabado;
                x.Saida = funcionario.Saida;
                x.Segunda = funcionario.Segunda;
                x.Status = funcionario.Status;
                x.Terca = funcionario.Terca;
                x.UF = funcionario.UF;
                x.Foto = funcionario.Foto;
            }
        }

   
    }

}
