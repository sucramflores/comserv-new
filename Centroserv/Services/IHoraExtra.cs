﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Centroserv.Services;
using Centroserv.Models;

namespace Centroserv.Services
{
    public interface IHoraExtra
    {
        HoraExtra Add(HoraExtra horaextra);
        IEnumerable<HoraExtra> GetAllByFuncionario(int idfuncionario);
        void Delete(int id);

    }

    public class InMemoryHoraExtra : IHoraExtra
    {
        private IClientes _clientes;
        private List<HoraExtra> _horaExtra;

        public InMemoryHoraExtra(IClientes clientes)
        {
            _clientes = clientes;
            _horaExtra = new List<HoraExtra>()
            {
                new HoraExtra{ Id = 1, Data="10/12/2018", IdCliente = 21, IdFuncionario = 3, ObsHoraExtra = "Cobriu o Joao que faltou", TotalHoras =2.5M},
                new HoraExtra{ Id = 2, Data="01/01/2019", IdCliente = 8, IdFuncionario = 3, ObsHoraExtra = "Multirao mensal", TotalHoras =3M},
                new HoraExtra{ Id = 3, Data="27/01/2019", IdCliente = 9, IdFuncionario = 3, ObsHoraExtra = "A pedido da proprietaria; Autorizado pela imobiliaria", TotalHoras =5.5M}
            };
        }

        public HoraExtra Add(HoraExtra horaextra)
        {
            if (_horaExtra.Count() > 0)
            {
                horaextra.Id = _horaExtra.Max(e => e.Id) + 1;
            }
            else
            {
                horaextra.Id = 1;
            }
            _horaExtra.Add(horaextra);
            return horaextra;
        }

        public void Delete(int id)
        {
            _horaExtra = _horaExtra.Where(e => e.Id != id).ToList();
        }

        public IEnumerable<HoraExtra> GetAllByFuncionario(int idfuncionario)
        {
            IEnumerable<HoraExtra> model = _horaExtra.Where(e => e.IdFuncionario == idfuncionario).OrderBy(e=>e.Id);
            foreach (var item in model)
            {
                item.Cliente = _clientes.Get(item.IdCliente);
            }
            return model;
        }

    }
}
