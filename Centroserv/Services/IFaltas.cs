﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Centroserv.Models;

namespace Centroserv.Services
{
    public interface IFaltas
    {
        Faltas Add(Faltas falta);
        void Delete(int id);
        void Update(Faltas falta);
        IEnumerable<Faltas> GetAllByFuncionario(int idFuncionario);
    }

    public class InMemoryFaltas : IFaltas
    {
        private List<Faltas> _faltas;

        public InMemoryFaltas()
        {
            _faltas = new List<Faltas>
            {
                new Faltas{ Data = "25/12/2018", Id = 1, IdFuncionario = 3, ObsFalta="fez o Ed. Sao Luiz e precisou sair apos passar mal. Usou VT" },
                new Faltas{ Data = "02/01/2019", Id = 2, IdFuncionario = 3, ObsFalta="Avo faleceu noite anterior. Avisou a Mariane as 6am" },
                new Faltas{ Data = DateTime.Now.ToString("dd/MM/yyyy"), Id = 3, IdFuncionario = 3, ObsFalta="Nao avisou; Faltou profoto e belle ville. Precisa descontar 2 VT" }

            };
        }

        public Faltas Add(Faltas falta)
        {
            if (_faltas.Count > 0) {
                falta.Id = _faltas.Max(e => e.Id) + 1;
            }
            else {
                falta.Id = 1;
            }
            _faltas.Add(falta);
            return falta;
        }

        public void Delete(int id)
        {
            _faltas = _faltas.Where(e => e.Id != id).ToList();
        }

        public IEnumerable<Faltas> GetAllByFuncionario(int idFuncionario)
        {
            return _faltas.Where(e => e.IdFuncionario == idFuncionario);

        }

        public void Update(Faltas falta)
        {
            foreach (var item in _faltas.Where(e => e.Id == falta.Id))
            {
                item.Data = falta.Data;
                item.ObsFalta = falta.ObsFalta;
            }
        }
    }
}
