﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Centroserv.Models;


namespace Centroserv.Services
{
    public interface IDespesas
    {
        IEnumerable<Despesas> GetAll(string ano, string mes);
        Despesas Get(int idDespesa);
        Despesas Add(Despesas despesa);
        void Update(Despesas despesa);
        void Delete(int idDespesa);

    }


    public class InMemoryDespesa: IDespesas
    {
        private List<Despesas> _despesa;

        public InMemoryDespesa()
        {
            _despesa = new List<Despesas>()
            {
                new Despesas { Id = 1, Data = "05/01/2018", Descricao = "Hora Extra (2h), a pedido da Ju", IdCliente = 13, TipoDespesa = Models.ValoresPadroes.TipoDespesa.Operacional, Valor = 36.00},
                new Despesas { Id = 2, Data = "11/01/2018", Descricao = "Funcionario quebrou por fotos", IdCliente = 13, TipoDespesa = Models.ValoresPadroes.TipoDespesa.Outros, Valor = 87.00},
                new Despesas { Id = 3, Data = "19/01/2018", Descricao = "Combustivel: 3 visistas no mesmo dia)", IdCliente = 13, TipoDespesa = Models.ValoresPadroes.TipoDespesa.Operacional, Valor = 17.00},
                new Despesas { Id = 4, Data = "19/01/2018", Descricao = "Hora extra, 10h", IdCliente = 12, TipoDespesa = Models.ValoresPadroes.TipoDespesa.Operacional, Valor = 147.00},
                new Despesas { Id = 5, Data = "21/02/2019", Descricao = "Hora extra, 2h", IdCliente = 12, TipoDespesa = Models.ValoresPadroes.TipoDespesa.Operacional, Valor = 45.00},
                new Despesas { Id = 6, Data = "11/01/2019", Descricao = "Hora extra, 4h", IdCliente = 12, TipoDespesa = Models.ValoresPadroes.TipoDespesa.Operacional, Valor = 76.00},
                new Despesas { Id = 7, Data = "03/03/2019", Descricao = "Diferenca salaria, dissidio 2019", IdCliente = 12, TipoDespesa = Models.ValoresPadroes.TipoDespesa.Rembolso, Valor = 411.50}
            };
        }

        public Despesas Add(Despesas despesa)
        {
            despesa.Id = _despesa.Max(e => e.Id) + 1;
            _despesa.Add(despesa);
            return despesa;
        }

        public void Delete(int idDespesa)
        {
           _despesa = _despesa.Where(e => e.Id != idDespesa).ToList();
        }

        public Despesas Get(int idDespesa)
        {
            return _despesa.FirstOrDefault(e => e.Id == idDespesa);
        }

        public IEnumerable<Despesas> GetAll(string ano, string mes)
        {
            if (ano != null && mes != null)
            {
                //return _despesa.Where(e => DateTime.ParseExact(e.Data,"d/M/yyyy", CultureInfo.InvariantCulture).Year == Convert.ToInt16(ano) && DateTime.ParseExact(e.Data, "d/M/yyyy", CultureInfo.InvariantCulture).Month == Convert.ToInt16(mes));                //return _despesa.Where(e => DateTime.ParseExact(e.Data,"d/M/yyyy", CultureInfo.InvariantCulture).Year == Convert.ToInt16(ano) && DateTime.ParseExact(e.Data, "d/M/yyyy", CultureInfo.InvariantCulture).Month == Convert.ToInt16(mes));
                return _despesa.Where(e => e.Data.Substring(e.Data.IndexOf("/")+1) == (mes+"/"+ano));
            }
            else
            {
                return _despesa;
            }
        }

        public void Update(Despesas despesa)
        {
            foreach(var i in _despesa.Where(e => e.Id == despesa.Id))
            {
                i.Data = despesa.Data;
                i.Descricao = despesa.Descricao;
                i.IdCliente = despesa.IdCliente;
                i.TipoDespesa = despesa.TipoDespesa;
                i.Valor = despesa.Valor;
            }

        }
    }
}
