﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Centroserv.Models;
using Centroserv.Models.ValoresPadroes;

namespace Centroserv.Services
{
    public interface IMaterial
    {
        IEnumerable<Material> GettAll();
        Material Add(Material model);
        Material Get(int id);
        void Delete(int id);
        void Update(Material model);
        double GetValorMaterial(int id);
    }

    public class InMemoryMaterial : IMaterial
    {
        private List<Material> _material;

        public InMemoryMaterial()
        {
            _material = new List<Material>
            {
                new Material{ Descricao = "Vassoura Comum", Id=1, QtdPadrao=1, Unidade="Unidade", ValorUnitario=2.3,DataUltimoReajuste = "01/02/2018"},
                new Material{ Descricao = "Pano Branco", Id=2, QtdPadrao=1, Unidade="Unidade", ValorUnitario=2.3,DataUltimoReajuste = "21/12/2015"},
                new Material{ Descricao = "Alvejante 1%", Id=3, QtdPadrao=0.5, Unidade="Litro", ValorUnitario=4.53,DataUltimoReajuste = "05/04/2018"},
                new Material{ Descricao = "Alvejante 5%", Id=5, QtdPadrao=2, Unidade="Litro", ValorUnitario=14.12,DataUltimoReajuste = "05/04/2018"},
                new Material{ Descricao = "Desinfetante Cruzado", Id=6, QtdPadrao=3, Unidade="Litro", ValorUnitario=48.55,DataUltimoReajuste = "05/04/2018"},
                new Material{ Descricao = "Desinfetante Pluron",Id=4, QtdPadrao=0.5, Unidade="Litro", ValorUnitario=12.0,DataUltimoReajuste = "24/05/2018"}

            };
        }

        public Material Add(Material model)
        {
            model.Id = _material.Max(e => e.Id) + 1;
            _material.Add(model);
            return model;
        }

        public void Delete(int id)
        {
            _material = _material.Where(e => e.Id != id).ToList();

        }

        public Material Get(int id)
        {
            return _material.FirstOrDefault(e => e.Id == id);
        }

        public IEnumerable<Material> GettAll()
        {
            return _material;
        }

        public double GetValorMaterial(int id)
        {
            var x = _material.FirstOrDefault(e => e.Id == id);
            return x.ValorUnitario;
        }

        public void Update(Material model)
        {
            foreach (var item in _material.Where(e=>e.Id==model.Id))
            {
                item.Descricao = model.Descricao;
                item.QtdPadrao = model.QtdPadrao;
                item.Unidade = model.Unidade;
                item.ValorUnitario = model.ValorUnitario;
                item.DataUltimoReajuste = model.DataUltimoReajuste;
            }
        }
    }
}
