﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Centroserv.Models;
using Centroserv.Models.ValoresPadroes;

namespace Centroserv.Services
{
    public interface IGrade
    {
        IEnumerable<Grade> GettAll(int? idCliente, int? idFuncionario, bool? ShowAtivosOnly);
        Grade Get(int id);
        Grade Add(Grade grade);
        void Update(Grade grade);
        IEnumerable<Grade> Delete(int id);
        void EncerraContrato(Grade grade);

    }

    public class InMemoryGrade : IGrade
    {
        private List<Grade> _grade;
        private IClientes _clientes;
        private IFuncionarios _funcionarios;

        public InMemoryGrade(IClientes clientes, IFuncionarios funcionarios)
        {
            _clientes = clientes;
            _funcionarios = funcionarios;

            _grade = new List<Grade>
            {
                new Grade { Id = 1, Ativo = true, Entrada = "08:00", IdCliente = 8, IdFuncionario=1, Inicio = DateTime.Now.ToString("dd/MM/yyyy"), Saida = "10:00", Dia = DiasGrade.Segunda},
                new Grade { Id = 2, Ativo = true, Entrada = "08:00", IdCliente = 8, IdFuncionario=1, Inicio = DateTime.Now.ToString("dd/MM/yyyy"), Saida = "10:00", Dia = DiasGrade.Quarta},
                new Grade { Id = 3, Ativo = true, Entrada = "08:00", IdCliente = 8, IdFuncionario=1, Inicio = DateTime.Now.ToString("dd/MM/yyyy"), Saida = "10:00", Dia = DiasGrade.Sexta},
                new Grade { Id = 4, Ativo = true, Entrada = "09:00", IdCliente = 9, IdFuncionario=2, Inicio = DateTime.Now.ToString("dd/MM/yyyy"), Saida = "11:00", Dia = DiasGrade.Terca},
                new Grade { Id = 5, Ativo = true, Entrada = "09:00", IdCliente = 9, IdFuncionario=2, Inicio = DateTime.Now.ToString("dd/MM/yyyy"), Saida = "11:30", Dia = DiasGrade.Sexta},
                new Grade { Id = 6, Ativo = false, Entrada = "08:00", IdCliente = 10, IdFuncionario=3, Inicio = DateTime.Now.ToString("dd/MM/yyyy"), Saida = "10:00",Dia = DiasGrade.Terca},
                new Grade { Id = 7, Ativo = false, Entrada = "08:00", IdCliente = 10, IdFuncionario=3, Inicio = DateTime.Now.ToString("dd/MM/yyyy"), Saida = "10:00",Dia = DiasGrade.Sexta},
                new Grade { Id = 8, Ativo = true, Entrada = "12:00", IdCliente = 11, IdFuncionario=4, Inicio = DateTime.Now.ToString("dd/MM/yyyy"), Saida = "17:59", Dia = DiasGrade.Sabado},
                new Grade { Id = 9, Ativo = true, Entrada = "18:00", IdCliente = 11, IdFuncionario=4, Inicio = DateTime.Now.ToString("dd/MM/yyyy"), Saida = "20:00", Dia = DiasGrade.Sabado},
                new Grade { Id = 10, Ativo = true, Entrada = "06:00", IdCliente = 8, IdFuncionario=4, Inicio = DateTime.Now.ToString("dd/MM/yyyy"), Saida = "08:00", Dia = DiasGrade.Sabado}
            };
        }

        public Grade Add(Grade grade)
        {
            grade.Id = _grade.Max(e => e.Id) + 1;
            _grade.Add(grade);
            return grade;
        }

         public void EncerraContrato(Grade grade)
        {
            throw new NotImplementedException();
        }

        public Grade Get(int id)
        {
            return _grade.FirstOrDefault(e => e.Id == id);
        }

        public IEnumerable<Grade> GettAll(int? idCliente, int? idFuncionario, bool? ShowAtivosOnly = true)
        {
            IEnumerable<Grade> repo;

            if (idCliente != null && idFuncionario != null)
            {
                repo = _grade.Where(e => e.IdCliente == idCliente && e.IdFuncionario == idFuncionario);
            }
            else if(idCliente != null && idFuncionario == null)
            {
                repo = _grade.Where(e => e.IdCliente == idCliente);
            }
            else if (idCliente == null && idFuncionario != null)
            {
                repo = _grade.Where(e => e.IdFuncionario == idFuncionario);
            }
            else if(ShowAtivosOnly == true)
            {
                repo = _grade.Where(e => (e.Fim == null) || (e.Fim == ""));
            }
            else
            {
                repo = _grade;
            }
                                                  
            foreach(var x in repo.OrderBy(r => r.IdCliente))
            {
                x.Funcionarios = _funcionarios.Get(x.IdFuncionario);
                x.Clientes = _clientes.Get(x.IdCliente);
            }
            return repo;
        }

        public void Update(Grade grade)
        {
            foreach(var x in _grade.Where(e => e.Id == grade.Id))
            {
                x.Ativo = true;
                x.Dia = grade.Dia;
                x.Entrada = grade.Entrada;
                x.Saida = grade.Saida;
                x.IdCliente = grade.IdCliente;
                x.IdFuncionario = grade.IdFuncionario;
                x.Inicio = grade.Inicio;
                x.Id = grade.Id;
                x.Fim = grade.Fim;
            }
        }

        public IEnumerable<Grade> Delete(int id)
        {
            _grade = _grade.Where(e => e.Id != id).ToList();
            return _grade;
        }
    }
}
