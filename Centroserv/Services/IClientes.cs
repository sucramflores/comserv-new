﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Centroserv.Models;
using Dapper;
using System.Configuration;
using Microsoft.Extensions.Configuration;

namespace Centroserv.Services
{
    public interface IClientes
    {
        IEnumerable<Clientes> GetAll();
        IEnumerable<Clientes> GettAllByImobiliaria(int id);
        Clientes Get(int id);
        Clientes Add(Clientes cliente);
        void RemoveImobiliaria(int idImobiliaria);
        void Update(Clientes cliente);
        void Delete(int id);
    }


    public class DBClientes : IClientes
    {
        private string _conectionString;
        private IDbConnection db;

        public DBClientes(IConfiguration config)
        {
            _conectionString = config.GetConnectionString("DefaultConnection");
            db = new SqlConnection(_conectionString);

        }

        public Clientes Add(Clientes cliente)
        {
            string sql = "Insert into clientes(alias,bairro,cidade,endereco,razao,uf,tipo," +
                "Contato,Equipamentos, Fim, NomeContato, EnderecoContato, FoneContato, CelularContato, EmailContato, Inicio, MaterialLimpeza, Observacao, " +
                "SerGramado,SerLimpeza,SerPortaria,SerZeladoria,selectedImobiliaria,ValorMaximoMaterial,latitude,longitude) values(@alias,@bairro,@cidade,@endereco,@razao,@uf,@tipo," +
                "@Contato, @Equipamentos, @Fim, @NomeContato, @EnderecoContato, @FoneContato, @CelularContato, @EmailContato, @Inicio, @MaterialLimpeza," +
                " @Observacao, @SerGramado, @SerLimpeza, @SerPortaria, @SerZeladoria, @selectedImobiliaria,@ValorMaximoMaterial,@Latitude,@Longitude); " +
                "select cast(scope_identity() as int)";
            var id = db.Query<int>(sql, cliente).Single();
            cliente.Id = id;
            return cliente;

        }

        public void Delete(int id)
        {
            string sql = "delete from clientes where id = @id";
            db.Execute(sql, new { id });
        }

        public Clientes Get(int id)
        {
            string sql = "select * from clientes where id = @id";
            return db.Query<Clientes>(sql, new { id }).FirstOrDefault();
        }

        public IEnumerable<Clientes> GetAll()
        {
            string sql = "Select * from clientes";
            return db.Query<Clientes>(sql).ToList();
        }

        public IEnumerable<Clientes> GettAllByImobiliaria(int id)
        {
            string sql = "Select * from clientes where selectedimobiliaria = @id";
            return db.Query<Clientes>(sql, new { id }).ToList();
        }

        public void RemoveImobiliaria(int idImobiliaria)
        {
            string sql = "update clientes " +
                "set selectedImobiliaria = 0 " +
                "where selectedImobiliaria = @idImobiliaria";
            db.Execute(sql, new { idImobiliaria = idImobiliaria });
        }

        public void Update(Clientes cliente)
        {
            string sql = "update clientes " +
                "set alias = @alias, " +
                    "bairro = @bairro, " +
                    "cidade = @cidade, " +
                    "endereco = @endereco, " +
                    "razao = @razao, " +
                    "uf = @uf, " +
                    "tipo = @tipo, " +
                    "Contato = @Contato, " +
                    "Equipamentos = @Equipamentos, " +
                    "Fim = @Fim, " +
                    "NomeContato = @NomeContato, " +
                    "EnderecoContato = @EnderecoContato, " +
                    "FoneContato = @FoneContato, " +
                    "CelularContato = @CelularContato, " +
                    "EmailContato = @EmailContato, " +
                    "Inicio = @Inicio, " +
                    "MaterialLimpeza = @MaterialLimpeza, " +
                    "Observacao = @Observacao, " +
                    "SerGramado = @SerGramado, " +
                    "SerLimpeza = @SerLimpeza, " +
                    "SerPortaria = @SerPortaria, " +
                    "SerZeladoria = @SerZeladoria, " +
                    "ValorMaximoMaterial = @ValorMaximoMaterial, " +
                    "Latitude = @Latitude, " +
                    "Longitude = @Longitude, " +
                    "selectedImobiliaria = @selectedImobiliaria " +
                "where id = @id";
            db.Execute(sql, cliente);



        }
    }

    //    public class InMemoryClientes : IClientes
    //    {
    //        List<Clientes> _clientes;

    //        public InMemoryClientes()
    //        {
    //            _clientes = new List<Clientes>
    //            {
    //                new Clientes { Alias = "Belle Ville", Bairro = "Centro", Cidade="Santa Maria", Endereco="Rua FLoriano Peixoto, 456", Id=1, Razao="Edificio Residencial Belle Ville", UF="RS", Tipo=TipoEstabelecimento.Condominio, Contato=TipoContato.Funcionario},
    //                new Clientes { Alias = "Torres Britanicas", Bairro = "Medianeira", Cidade="Santa Maria", Endereco="Av. Fernando Ferrari, 456", Id=2, Razao="Edificio Residencial Torres Britanicas", UF="RS", Tipo=TipoEstabelecimento.Condominio,Contato=TipoContato.Subsindico},
    //                new Clientes { Alias = "Arnaldo Y Castro", Bairro = "Centro", Cidade="Santa Maria", Endereco="Rua Andre Marques, 412", Id=3, Razao="Residencial Arnaldo Y Castro", UF="RS", Tipo=TipoEstabelecimento.Condominio,Contato=TipoContato.Sindico},
    //                new Clientes { Alias = "Mainardi", Bairro = "Camobi", Cidade="Santa Maria", Endereco="RST 4500, 98", Id=4, Razao="Edificio Residencial e Comercial Mainardi", UF="RS", Tipo=TipoEstabelecimento.Outro,Contato=TipoContato.Morador},
    //                new Clientes { Alias = "Jupiter", Bairro = "Rosario", Cidade="Santa Maria", Endereco="Rua Dona Luiza, 74", Id=5, Razao="Edificio Residencial Jupiter", UF="RS", Tipo=TipoEstabelecimento.Condominio,Contato=TipoContato.Morador},
    //            };
    //        }


    //        public Clientes Add(Clientes cliente)
    //        {
    //            cliente.Id = _clientes.Max(r => r.Id) + 1;
    //            _clientes.Add(cliente);
    //            return cliente;

    //        }

    //        public IEnumerable<Clientes> Delete(int id)
    //        {
    //            _clientes = _clientes.Where(r => r.Id != id).ToList();
    //            return _clientes;
    //        }

    //        public Clientes Get(int idclie)
    //        {
    //            return _clientes.FirstOrDefault(r => r.Id == idclie);
    //        }

    //        public IEnumerable<Clientes> GetAll()
    //        {
    //            return _clientes.OrderBy(r => r.Alias);
    //        }

    //        public Clientes Update(Clientes cliente)
    //        {

    //            foreach (var e in _clientes.Where(a => a.Id == cliente.Id)) {
    //                e.Alias = cliente.Alias;
    //                e.Bairro = cliente.Bairro;
    //                e.Cidade = cliente.Cidade;
    //                e.Endereco = cliente.Endereco;
    //                e.Razao = cliente.Razao;
    //                e.Tipo = cliente.Tipo;
    //                e.UF = cliente.UF;
    //                e.SelectedImobiliaria = cliente.SelectedImobiliaria;
    //                e.Contato = cliente.Contato;
    //            }

    //            return cliente;

    //        }
    //    }
}
