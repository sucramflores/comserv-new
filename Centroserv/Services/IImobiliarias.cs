﻿using Centroserv.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Centroserv.Services
{
    public interface IImobiliarias
    {
        IEnumerable<Imobiliarias> GetAll();
        Imobiliarias Get(int id);
        Imobiliarias Add(Imobiliarias imobiliaria);
        void Update(Imobiliarias imobiliaria);
        IEnumerable<Imobiliarias> Delete(int id);
    }


    public class InMemoryImobiliairas : IImobiliarias
    {

        List<Imobiliarias> _imobiliairas;

        public InMemoryImobiliairas()
        {
            _imobiliairas = new List<Imobiliarias>
            {
                new Imobiliarias{ Bairro = "Centro", Cidade="Santa Maria", Contato="Michele", Email="contato@novaera.com.br", Endereco="Rua Venancio Aires, 45", Fone="(55)3226-2626", Id = 1, Nome="Nova Era", Obs="Michele esta de Ferias"},
                new Imobiliarias{ Bairro = "Camobi", Cidade="Santa Maria", Contato="Sydnei", Email="cond@bancodeimoveis.com.br", Endereco="RST 287, 7730", Fone="(55)3226-4878", Id = 2, Nome="Banco de Imoveis ", Obs="Mandar boletos por email"},
                new Imobiliarias{ Bairro = "Centro", Cidade="Santa Maria", Contato="Carlos", Email="carlos@cruzeiro.com.br", Endereco="Rua Venancio Aires, 78", Fone="(55)3220-7154", Id = 3, Nome="Cruzeiro", Obs="Carlos mudou p/ extensao 105"},
                new Imobiliarias{ Bairro = "Centro", Cidade="Santa Maria", Contato="Cleide", Email="cleide@JCLemos.com.br", Endereco="Rua Andre Marques, 78", Fone="(55)3220-7844", Id = 4, Nome="JC Lemos", Obs="Novos donos"},
                new Imobiliarias{ Bairro = "Centro", Cidade="Santa Maria", Contato="Beatris", Email="basm@bol.com.br", Endereco="Rua Venancio Aires, 1278", Fone="(55)2703-9876", Id = 5, Nome="Jair Behr", Obs="Beatriz esta de ferias"}

            };
        }


        public Imobiliarias Add(Imobiliarias imobiliaria)
        {
            int newId = _imobiliairas.Max(e => e.Id) + 1;
            imobiliaria.Id = newId;
            _imobiliairas.Add(imobiliaria);
            return imobiliaria;
        }

        public IEnumerable<Imobiliarias> Delete(int id)
        {
            _imobiliairas = _imobiliairas.Where(e => e.Id != id).ToList();
            return _imobiliairas;
        }

        public Imobiliarias Get(int id)
        {
            var imob = _imobiliairas.FirstOrDefault(e => e.Id == id);
            return imob;
        }

        public IEnumerable<Imobiliarias> GetAll()
        {
            return _imobiliairas.OrderBy(n => n.Nome);

        }

        public void Update(Imobiliarias imobiliaria)
        {
            foreach(var x in _imobiliairas.Where(e => e.Id == imobiliaria.Id))
            {
                x.Bairro = imobiliaria.Bairro;
                x.Cidade = imobiliaria.Cidade;
                x.Contato = imobiliaria.Contato;
                x.Email = imobiliaria.Email;
                x.Endereco = imobiliaria.Endereco;
                x.Fone = imobiliaria.Fone;
                x.Nome = imobiliaria.Nome;
                x.UF = imobiliaria.UF;
            }
        }
    }
}
