﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Centroserv.Services;
using Centroserv.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Centroserv.Models;
using Microsoft.AspNetCore.Authorization;

namespace Centroserv
{
    [Authorize]
    public class ImobiliariasController : Controller
    {
        private IImobiliarias _imobiliarias;
        private IClientes _clientes;

        public ImobiliariasController(IImobiliarias imobiliariasData, IClientes clientes)
        {
            _imobiliarias = imobiliariasData;
            _clientes = clientes;

        }

        //[Authorize]
        public IActionResult Index()
        {
            var model = new ImobiliariasIndexViewModel();
            model.Imobiliarias = _imobiliarias.GetAll();
            return View(model);
        }

        [HttpGet]
        public IActionResult Edit(int id)
        {
            var imobiliaria = _imobiliarias.Get(id);
            var model = new ImobiliariasEditViewIndex();
            model.Bairro = imobiliaria.Bairro;
            model.Cidade = imobiliaria.Cidade;
            model.Contato = imobiliaria.Contato;
            model.Email = imobiliaria.Email;
            model.Endereco = imobiliaria.Endereco;
            model.Fone = imobiliaria.Fone;
            model.Id = imobiliaria.Id;
            model.Nome = imobiliaria.Nome;
            model.Obs = imobiliaria.Obs;
            model.UF = imobiliaria.UF;
            model.Clientes = _clientes.GettAllByImobiliaria(id);
            return View(model);
        }


        [HttpPost]
        public IActionResult Edit(ImobiliariasEditViewIndex imobiliaria)
        {
            var model = new Imobiliarias();
            model.Bairro = imobiliaria.Bairro;
            model.Cidade = imobiliaria.Cidade;
            model.Contato = imobiliaria.Contato;
            model.Email = imobiliaria.Email;
            model.Endereco = imobiliaria.Endereco;
            model.Fone = imobiliaria.Fone;
            model.Id = imobiliaria.Id;
            model.Nome = imobiliaria.Nome;
            model.Obs = imobiliaria.Obs;
            model.UF = imobiliaria.UF;
            _imobiliarias.Update(model);
            return RedirectToAction("Index");
        }


        [HttpGet]
        public IActionResult Del(int id)
        {
            //int total = _clientes.GettAllByImobiliaria(id).Count();
            _clientes.RemoveImobiliaria(id);
            _imobiliarias.Delete(id);
            return RedirectToAction(nameof(Index));

        }


        [HttpGet]
        public IActionResult create()
        {
            return View();
        }


        [HttpPost]
        public IActionResult create(ImobiliariasEditViewIndex imob)
        {
            var model = new Imobiliarias();
            model.Bairro = imob.Bairro;
            model.Cidade = imob.Cidade;
            model.Contato = imob.Contato;
            model.Email =  imob.Email;
            model.Endereco = imob.Endereco;
            model.Fone = imob.Fone;
            model.Nome = imob.Nome;
            model.Obs = imob.Obs;
            model.UF = imob.UF;
            _imobiliarias.Add(model);
            return RedirectToAction(nameof(Index));
        }
    }
}