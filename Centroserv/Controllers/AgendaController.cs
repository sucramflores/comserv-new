﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Centroserv.Models;
using Centroserv.Services;
using Centroserv.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace Centroserv.Controllers
{
    [Route("agenda/{id?}")]
    public class AgendaController : Controller
    {
        public IEntregas _entregas;
        public IMaterial _material;
        public IClientes _clientes;
        public IServicos _servicos { get; set; }

        public AgendaController(IEntregas entrega, IMaterial material, IClientes clientes, IServicos servicos)
        {
            _entregas = entrega;
            _material = material;
            _clientes = clientes;
            _servicos = servicos;
        }

        public IActionResult Index(string id)
        {
            ViewBag.parametro = id; //data da busca (não ID)
            DateTime DtDate;
            if (!string.IsNullOrEmpty(id))
            {
                var date = id.Substring(2, 2) +"/"+ id.Substring(0, 2) + "/" + id.Substring(4,4);  // data americana
                DtDate = Convert.ToDateTime(date);
                ViewBag.Data = id.Substring(0, 2) + "/" + id.Substring(2, 2) ;
            }
            else
            {
                DtDate = DateTime.Now.Date;
            }
            var model = new AgendaIndexViewModel();
            model.Servicos = _servicos.GetAll(DtDate);
            foreach(var y in model.Servicos)
            {
                y.Cliente = _clientes.Get(y.IdCliente);
            }

            model.Entregas = _entregas.GetAll(DtDate,false);
            foreach (var x in model.Entregas)
            {

                x.Cliente = _clientes.Get(x.IdCliente); // get the Client for this delivery object
                foreach(EntregaItens i in x.Itens)
                {
                    i.Material = _material.Get(i.IdItem);
                }
            }
            model.MaterialAll = _material.GettAll();
            ViewBag.Clientes = _clientes.GetAll(); 
            model.Clientes = ViewBag.Clientes;
            return View(model);
        }

       

    }
}