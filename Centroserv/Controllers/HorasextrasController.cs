﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Centroserv.ViewModels;
using Centroserv.Models;
using Centroserv.Services;

namespace Centroserv.Controllers
{
    public class HorasextrasController : Controller
    {
        private IHoraExtra _horasextras;

        public HorasextrasController(IHoraExtra horae)
        {
            _horasextras = horae; 
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(HoraExtraCreateViewModel horaextra)
        {
            var model = new HoraExtra()
            {
                Data = horaextra.Data,
                IdCliente = horaextra.IdCliente,
                IdFuncionario = horaextra.IdFuncionario,
                ObsHoraExtra = horaextra.ObsHoraExtra,
                TotalHoras = horaextra.TotalHoras,
            };
            _horasextras.Add(model);
            return RedirectToAction("Edit", "Funcionarios", new { id = horaextra.IdFuncionario });
        }

        [Route("HE/delete/{id}/{idfuncionario}")]
        public IActionResult Delete(int id, int idfuncionario)
        {
            _horasextras.Delete(id);
            return RedirectToAction("Edit", "Funcionarios", new { id = idfuncionario });
        }
    }
}