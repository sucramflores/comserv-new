﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Centroserv.Models;
using Centroserv.Services;
using Centroserv.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace Centroserv.Controllers
{
    public class ServicosController : Controller
    {
        private IServicos _servicos;

        public ServicosController(IServicos servic)
        {
            _servicos = servic;
        }
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(ServicoCreateViewModal servico)
        {
            if (ModelState.IsValid)
            {
                var model = new Servicos();
                _servicos.Add(model);
            }
            return RedirectToAction(nameof(Index),"Agenda");

        }
    }
}