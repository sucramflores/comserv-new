﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Centroserv.Services;
using Centroserv.ViewModels;
using Centroserv.Models;

namespace Centroserv.Controllers
{
    public class FuncionariosController : Controller
    {
        private IFuncionarios _funcionarios;
        private IFaltas _faltas;
        private IClientes _clientes;
        private IHoraExtra _horasExtra;

        public FuncionariosController(IFuncionarios funcionarios,IFaltas faltas, IClientes clientes,IHoraExtra hora)
        {
            _funcionarios = funcionarios;
            _faltas = faltas;
            _clientes = clientes;
            _horasExtra = hora;
        }


        public IActionResult Index()
        {
            var model = new FuncionariosIndexViewModel();
            model.Funcionarios = _funcionarios.GetAll();
            return View(model);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(FuncionariosEditViewModel func)
        {
            var model = new Funcionarios()
            {
                Admissao = func.Admissao,
                Alias = func.Alias,
                Bairro = func.Bairro,
                Cargo = func.Cargo,
                Celular = func.Celular,
                Cidade = func.Cidade,
                ContatoEmergencia = func.ContatoEmergencia,
                ContatoEmergenciaCelular = func.ContatoEmergenciaCelular,
                Endereco = func.Endereco,
                Fone = func.Fone,
                IndicadaPor = func.IndicadaPor,
                Nascimento = func.Nascimento,
                Nome = func.Nome,
                Obs = func.Obs,
                Saida = func.Saida,
                Status = func.Status,
                UF = func.UF,
                Segunda = func.Segunda,
                Terca = func.Terca,
                Quarta = func.Quarta,
                Quinta = func.Quinta,
                Sexta = func.Sexta,
                Sabado = func.Sabado,
                Domingo = func.Domingo
                
            };
            _funcionarios.Add(model);
            return RedirectToAction(nameof(Index));
        }

        [HttpGet]
        public IActionResult Edit(int id)
        {
            var func = _funcionarios.Get(id);
            var model = new FuncionariosEditViewModel
            {
                Admissao = func.Admissao,
                Alias = func.Alias,
                Bairro = func.Bairro,
                Cargo = func.Cargo,
                Celular = func.Celular,
                Cidade = func.Cidade,
                ContatoEmergencia = func.ContatoEmergencia,
                ContatoEmergenciaCelular = func.ContatoEmergenciaCelular,
                Endereco = func.Endereco,
                Fone = func.Fone,
                IndicadaPor = func.IndicadaPor,
                Nascimento = func.Nascimento,
                Nome = func.Nome,
                Obs = func.Obs,
                Saida = func.Saida,
                Status = func.Status,
                UF = func.UF,
                Segunda = func.Segunda,
                Terca = func.Terca,
                Quarta = func.Quarta,
                Quinta = func.Quinta,
                Sexta = func.Sexta,
                Sabado = func.Sabado,
                Domingo = func.Domingo,
                Id = func.Id,
                Faltas = _faltas.GetAllByFuncionario(func.Id),
                ClientesTodos = _clientes.GetAll(),
                HorasExtras = _horasExtra.GetAllByFuncionario(id),
                Foto = func.Foto
            };

            return View(model);
        }


        [HttpPost]
        public IActionResult Edit(FuncionariosEditViewModel funcionario)
        {
            var model = new Funcionarios()
            {
                Admissao = funcionario.Admissao,
                Alias = funcionario.Alias,
                Bairro = funcionario.Bairro,
                Cargo = funcionario.Cargo,
                Celular = funcionario.Celular,
                Cidade = funcionario.Cidade,
                ContatoEmergencia = funcionario.ContatoEmergencia,
                ContatoEmergenciaCelular = funcionario.ContatoEmergenciaCelular,
                Domingo = funcionario.Domingo,
                Endereco = funcionario.Endereco,
                Fone = funcionario.Fone,
                IndicadaPor = funcionario.IndicadaPor,
                Nascimento = funcionario.Nascimento,
                Nome = funcionario.Nome,
                Obs = funcionario.Obs,
                Quarta = funcionario.Quarta,
                Quinta = funcionario.Quinta,
                Sexta = funcionario.Sexta,
                Sabado = funcionario.Sabado,
                Saida = funcionario.Saida,
                Segunda = funcionario.Segunda,
                Status = funcionario.Status,
                Terca = funcionario.Terca,
                UF = funcionario.UF,
                Id = funcionario.Id
            };
            _funcionarios.Update(model);
            return RedirectToAction(nameof(Index));
        }

        public IActionResult Delete(int id)
        {
            _funcionarios.Delete(id);
            return RedirectToAction(nameof(Index));
        }
    }
}