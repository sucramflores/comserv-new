﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Centroserv.Services;
using Centroserv.ViewModels;
using Centroserv.Models;


namespace Centroserv.Controllers
{
    public class MaterialController : Controller
    {
        private IMaterial _material;

        public MaterialController(IMaterial material)
        {
            _material = material;
        }

        // GET: Material/
        public ActionResult Index()
        {
            var model = new MaterialIndexViewModel()
            {
                Materiais = _material.GettAll()
            };

            return View(model);
        }

        // GET: Material/Details/5
        public ActionResult Details(int id)
        {
            var material = _material.Get(id);
            var model = new MaterialEditViewModel()
            {
                Id = material.Id,
                Descricao = material.Descricao,
                QtdPadrao = material.QtdPadrao,
                Unidade = material.Unidade,
                ValorUnitario = material.ValorUnitario
            };
            return View(model);
        }

        // GET: Material/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Material/Create
        [HttpPost]
        public ActionResult Create(MaterialEditViewModel newMaterial)
        {

            try
            {
                var model = new Material()
                {
                    Id = newMaterial.Id,
                    Descricao = newMaterial.Descricao,
                    QtdPadrao = newMaterial.QtdPadrao,
                    Unidade = newMaterial.Unidade,
                    ValorUnitario = newMaterial.ValorUnitario,
                    DataUltimoReajuste = DateTime.Now.ToString("dd/MM/yyyy")
                };
                _material.Add(model);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Material/Edit/5
        public ActionResult Edit(int id)
        {
            var material = _material.Get(id);
            var model = new MaterialEditViewModel()
            {
                Id = material.Id,
                Descricao = material.Descricao,
                QtdPadrao = material.QtdPadrao,
                Unidade = material.Unidade,
                ValorUnitario = material.ValorUnitario,
                DataUltimoReajuste = material.DataUltimoReajuste
            };
            return View(model);
        }

        // POST: Material/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                var model = new Material()
                {
                    Id = id,
                    Descricao = collection["Descricao"],
                    QtdPadrao = Convert.ToDouble(collection["QtdPadrao"]),
                    Unidade = collection["Unidade"],
                    ValorUnitario = Convert.ToDouble(collection["ValorUnitario"]),
                };

                if (_material.GetValorMaterial(id) != Convert.ToDouble(collection["ValorUnitario"]))//checka se houve mudanca
                {
                    model.DataUltimoReajuste = DateTime.Now.ToString("dd/MM/yyyy");
                }
                else
                {
                    model.DataUltimoReajuste = collection["DataUltimoReajuste"];
                }

                _material.Update(model);

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Material/Delete/5
        public ActionResult Delete(int id)
        {
            _material.Delete(id);
            return RedirectToAction(nameof(Index));
        }


    }
}