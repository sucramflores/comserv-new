﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Centroserv.Models;
using Centroserv.Services;
using Centroserv.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Centroserv.Controllers
{
    public class EntregasController : Controller
    {

        private IClientes _cliente;
        private IEntregas _entrega;
        private IMaterial _material;

        public EntregasController(IClientes cliente, IEntregas entrega, IMaterial material)
        {
            _cliente = cliente;
            _entrega = entrega;
            _material = material;
        }

        //[Route("entregas/{exibeTudo?}")]
        public IActionResult Index(string id)
        {
            var model2 = new AgendaIndexViewModel();
            model2.Clientes = _cliente.GetAll();
            model2.MaterialAll = _material.GettAll();

            if (id != null)
            {
                model2.Entregas = _entrega.GetAll(DateTime.Now, true);
            }
            else
            {
                model2.Entregas = _entrega.GetAll(DateTime.Now, true).Where(e=>e.Entregue != true);
            }
            foreach (var x in model2.Entregas)
            {
                x.Cliente = _cliente.Get(x.IdCliente);
                x.Itens = _entrega.GetAllItems(x.IdEntrega);
                foreach(var n in x.Itens)
                {
                    n.Material = _material.Get(n.IdItem);

                }
            }
            return View(model2);
        }


        [HttpPost]
        //public ActionResult Create(IFormCollection form)
        public ActionResult Create(EntregaCreateModel entrega)
        {
            Entregas model = new Entregas();
            model.DataEntrega = entrega.dataEntrega;
            model.IdCliente = entrega.txtClienteID;
            model.Obs = entrega.txtObservacao;
            model.Solicitado = DateTime.Now.Date;
            model = _entrega.Add(model);

            for (int i = 0; i < entrega.itemID.Count(); i++) { // o itemID recebido como array uma vez que há N+ itens
                EntregaItens model2 = new EntregaItens();
                model2.IdItem = entrega.itemID[i] ;
                model2.Quantidade = entrega.itemQuantidade[i];
                model2.IdEntrega = model.IdEntrega;
                _entrega.AddItem(model.IdEntrega, model2);
                model2 = null;
            }
            


            return RedirectToAction(nameof(Index),"Agenda");
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            if (id != 0) {
                var model = _entrega.Get(id);
                //model.Itens = _entrega.GetAllItems(id);
                model.Cliente = _cliente.Get(model.IdCliente);
                ViewBag.MaterialAll = _material.GettAll();
                ViewBag.Clientes = _cliente.GetAll();

                return View(model);
            }
            else
            {
                return RedirectToAction(nameof(Index));
            }
            
        }

        [HttpPost]
        public ActionResult Edit(IFormCollection form)
        {
            Entregas model = new Entregas();
            //model.CustoTotal = decimal.Parse(form["CustoTotal"]);
            model.IdEntrega = int.Parse(form["IdEntrega"]);
            model.DataEntrega = Convert.ToDateTime(form["DataEntrega"]);
            model.Obs = form["Obs"];
            model.Entregue = Convert.ToBoolean(form["Entregue"][0]);

            _entrega.RemoveAllItens(model.IdEntrega);
            EntregaItens modelItens = new EntregaItens();

            model.Itens = new List<EntregaItens>();
            for (int x = 0; x < form["itemID"].Count; x++)
            {
                modelItens.IdEntrega = model.IdEntrega;
                modelItens.IdItem = Convert.ToInt16(form["itemID"][x]);
                modelItens.Quantidade = Convert.ToDecimal(form["itemQuantidade"][x]);
                model.Itens.Add(modelItens);
                _entrega.AddItem(model.IdEntrega, modelItens);    
            }
            _entrega.Update(model);


            return RedirectToAction(nameof(Index),"Entregas");
        }

        [HttpPost]
        public ActionResult Delete(IFormCollection form)
        {
            var tempId = Convert.ToInt16(form["txtIdToDelete"]);
            _entrega.Remove(tempId);
            _entrega.RemoveAllItens(tempId);
            return RedirectToAction(nameof(Index), "Entregas");
        }

    }
}