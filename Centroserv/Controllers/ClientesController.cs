﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Centroserv.Models;
using Centroserv.Services;
using Centroserv.ViewModels;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Centroserv
{
    public class ClientesController : Controller
    {
        // GET: /<controller>/

        private IClientes _clientes;
        private IImobiliarias _imobiliarias;

        public ClientesController(IClientes clienteData, IImobiliarias imobiliariasData)
        {
            _clientes = clienteData;
            _imobiliarias = imobiliariasData;
        }

        public IActionResult Index()
        {
            var model = new ClientesIndexViewModel();
            model.Clientes = _clientes.GetAll();

            return View(model);
        }

        [HttpGet]
        public IActionResult Create()
        {
            var model = new ClientesEditViewModel();
            model.ListaImobiliarias = _imobiliarias.GetAll().ToList();

            return View(model);
        }

        [HttpGet]
        public IActionResult Edit(int id)
        {
            if (id != 0)
            {
                var clie = _clientes.Get(id);
                if (clie == null)
                {
                    NotFound();
                }
                var model = new ClientesEditViewModel();
                model.Alias = clie.Alias;
                model.Bairro = clie.Bairro;
                model.Cidade = clie.Cidade;
                model.Endereco = clie.Endereco;
                model.Razao = clie.Razao;
                model.Tipo = clie.Tipo;
                model.UF = clie.UF;
                model.SelectedImobiliaria = clie.SelectedImobiliaria;
                model.ListaImobiliarias = _imobiliarias.GetAll().ToList();

                model.Contato = clie.Contato;
                model.Equipamentos = clie.Equipamentos;
                model.Fim = clie.Fim;
                model.NomeContato = clie.NomeContato;
                model.EnderecoContato = clie.EnderecoContato;
                model.FoneContato = clie.FoneContato;
                model.CelularContato = clie.CelularContato;
                model.EmailContato = clie.EmailContato;
                model.Inicio = clie.Inicio;
                model.MaterialLimpeza = clie.MaterialLimpeza;
                model.Observacao = clie.Observacao;
                model.SerGramado = clie.SerGramado;
                model.SerLimpeza = clie.SerLimpeza;
                model.SerPortaria = clie.SerPortaria;
                model.SerZeladoria = clie.SerZeladoria;
                model.ValorMaximoMaterial = clie.ValorMaximoMaterial;
                model.Latitude = clie.Latitude;
                model.Longitude = clie.Longitude;
                return View(model);
            }

            return RedirectToAction(nameof(Index));
        }





        [HttpPost]
        public IActionResult Edit(Clientes cli)
        {
            if (ModelState.IsValid)
            {
                _clientes.Update(cli);
                return RedirectToAction("Index"); // Redirect is a common practice to avoid the user to refresh the browser and resubmit the form
            }
            else
            {
                return View();
            }
        }








        public IActionResult Delete(int id)
        {
            _clientes.Delete(id);
            return RedirectToAction(nameof(Index));
        }










        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Clientes model)
        {
            if (ModelState.IsValid) {
                var newCliente = new Clientes();
                newCliente.Alias = model.Alias;
                newCliente.Bairro = model.Bairro;
                newCliente.Cidade = model.Cidade;
                newCliente.Endereco = model.Endereco;
                newCliente.Razao = model.Razao;
                newCliente.Tipo = model.Tipo;
                newCliente.UF = model.UF;
                newCliente.SelectedImobiliaria = model.SelectedImobiliaria;
                newCliente.Contato = model.Contato;
                newCliente.EmailContato = model.EmailContato;
                newCliente.CelularContato = model.CelularContato;
                newCliente.Contato = model.Contato;
                newCliente.EnderecoContato = model.EnderecoContato;
                newCliente.Equipamentos = model.Equipamentos;
                newCliente.Fim = model.Fim;
                newCliente.FoneContato = model.FoneContato;
                newCliente.Inicio = model.Inicio;
                newCliente.MaterialLimpeza = model.MaterialLimpeza;
                newCliente.NomeContato = model.NomeContato;
                newCliente.Observacao = model.Observacao;
                newCliente.SerGramado = model.SerGramado;
                newCliente.SerLimpeza = model.SerLimpeza;
                newCliente.SerPortaria = model.SerPortaria;
                newCliente.SerZeladoria = model.SerZeladoria;
                newCliente.ValorMaximoMaterial = model.ValorMaximoMaterial;



                newCliente = _clientes.Add(newCliente);

                //return RedirectToAction(nameof(Edit), new { id = newCliente.Id });
                return RedirectToAction(nameof(Index));

            }
            else
            {
                return View();
            }

        }
    }
}
