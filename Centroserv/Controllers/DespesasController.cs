﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Centroserv.Services;
using Centroserv.ViewModels;
using Centroserv.Models;
using Centroserv.Models.ValoresPadroes;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;

namespace Centroserv.Controllers
{
    
    public class DespesasController : Controller
    {
        private IDespesas _despesa;
        private IClientes _clientes;

        
        public DespesasController(IDespesas despesa, IClientes clientes)
        {
            _despesa = despesa;
            _clientes = clientes;
        }


        // GET: Despesas
        [Route("/despesas/{ano?}/{mes?}")]
        public ActionResult Index(string ano, string mes)
        {
            var model = new DespesasIndexViewModel();
            model.Despesas = _despesa.GetAll(ano,mes);
            foreach(var i in model.Despesas)
            {
                i.Cliente = _clientes.Get(i.IdCliente);
            }

            int auxMes, auxAno;

            bool flagMes = int.TryParse(mes, out auxMes); 
            bool flagAno = int.TryParse(ano, out auxAno);

            if (!flagAno || !flagMes)
            {
                ViewBag.Ano = DateTime.Now.Year;
                //ViewBag.Mes = DateTime.Now.Month;
            }
            else
            {
                ViewBag.Mes = auxMes;
                ViewBag.Ano = auxAno;
            }
            model.ListaClientes = _clientes.GetAll();
            return View(model);
        }

        // GET: Despesas/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }



        // POST: Despesas/Create
        [Route("/despesas/create/")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            var model = new Despesas();
            model.IdCliente = Convert.ToInt16(collection["IdCliente"]);
            model.TipoDespesa = (TipoDespesa)Enum.Parse(typeof(TipoDespesa), collection["TipoDespesa"]);
            int.TryParse(collection["Valor"], out int newValor);
            model.Valor = newValor;
            model.Descricao = collection["Descricao"];
            model.Data = collection["Data"];
            _despesa.Add(model);
            return RedirectToAction(nameof(Index));
        }

        // GET: Despesas/Edit/5
        [Route("api/despesas/edit/{id}")]
        public ActionResult Edit(int id)
        {
            var model = _despesa.Get(id);
            string modelJson = JsonConvert.SerializeObject(
                new
                {
                    dados = model
                });
            return Ok(modelJson);
        }

        // POST: Despesas/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }


        [Route("/despesas/delete/")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(string id, IFormCollection collection)
        {
            _despesa.Delete(Convert.ToInt16(collection["idToBeDeleted"]));
            return RedirectToAction(nameof(Index));
        }
    }
}