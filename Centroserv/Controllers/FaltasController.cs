﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Centroserv.ViewModels;
using Centroserv.Models;
using Centroserv.Services;

namespace Centroserv.Controllers
{
    public class FaltasController : Controller
    {
        private IFaltas _faltas;

        public FaltasController(IFaltas falta)
        {
            _faltas = falta;
        }


        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(FaltasEditViewModel falta)
        {
            var model = new Faltas()
            {
                Data = falta.Data,
                IdFuncionario = falta.IdFuncionario,
                ObsFalta = falta.ObsFalta
            };
            _faltas.Add(model);
            return RedirectToAction("Edit","Funcionarios",new { id = model.IdFuncionario });
        }

        [Route("faltas/delete/{id}/{idfuncionario}")]
        public IActionResult Delete(int id,int idfuncionario)
        {
            _faltas.Delete(id);
            return RedirectToAction("Edit", "Funcionarios", new { id = idfuncionario });
        }
    }
}