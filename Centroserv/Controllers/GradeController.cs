﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Centroserv.Services;
using Centroserv.ViewModels;
using Centroserv.Models;

namespace Centroserv.Controllers
{
    public class GradeController : Controller
    {
        private IGrade _grade;
        private IFuncionarios _funcionarios;
        private IClientes _clientes;

        public GradeController(IGrade grade, IFuncionarios funcionarios, IClientes clientes)
        {
            _grade = grade;
            _funcionarios = funcionarios;
            _clientes = clientes;
        }

        
        //[Route("")]
        [Route("grade/index/{MostraTodos?}")]
        public IActionResult Index(string MostraTodos) //id aqui eh usado como flag (mostra todos os horarios ou so ativos)
        {
            var model = new GradeIndexViewModel();
            if (string.IsNullOrEmpty(MostraTodos))
            {
                model.GradeDeHorario = _grade.GettAll(null, null, true);
            }
            else
            {
                model.GradeDeHorario = _grade.GettAll(null, null, false);
            }
            //var ClienteDistinct = model.GradeDeHorario.Select(e => new { e.IdFuncionario, e.IdCliente }).Distinct();
            var ClienteDistinct = from x in model.GradeDeHorario
                                  group x by new { x.IdCliente, x.IdFuncionario } into newGroup
                                  orderby newGroup.Key.IdFuncionario
                                  select newGroup;
                                  
         
            model.GradeClientesUnico = ClienteDistinct;
            model.FuncionariosTodos = _funcionarios.GetAll();
            model.ClientesTodos = _clientes.GetAll();
            return View(model);
        }


        [HttpGet]
        public IActionResult Del(int id)
        {
            var model = _grade.Delete(id);
            return RedirectToAction(nameof(Index));
        }




        [HttpGet]
        public IActionResult Edit(int id)
        {
            var model = new GradeIndexViewModel();
            model.GradeDeHorario = _grade.GettAll(null, null, null);
            //var ClienteDistinct = model.GradeDeHorario.Select(e => new { e.IdFuncionario, e.IdCliente }).Distinct();
            var ClienteDistinct = from x in model.GradeDeHorario
                                  group x by new { x.IdCliente, x.IdFuncionario } into newGroup
                                  orderby newGroup.Key.IdFuncionario
                                  select newGroup;

            model.GradeClientesUnico = ClienteDistinct;
            model.FuncionariosTodos = _funcionarios.GetAll();
            model.ClientesTodos = _clientes.GetAll();
            model.gradeSelected = _grade.Get(id);
            return View(model);
        }


        [HttpPost]
        public IActionResult Edit(GradeIndexViewModel grade)
        {
            var model = new Grade();
            model.Id = grade.gradeSelected.Id;
            model.Ativo = true;
            model.Dia = grade.gradeSelected.Dia;
            model.IdFuncionario = grade.gradeSelected.IdFuncionario;
            model.IdCliente = grade.gradeSelected.IdCliente;
            model.Entrada = grade.gradeSelected.Entrada;
            model.Saida = grade.gradeSelected.Saida;
            model.Inicio = grade.gradeSelected.Inicio;
            model.Fim = grade.gradeSelected.Fim;
            _grade.Update(model);
            return RedirectToAction(nameof(Index));
        }



        [HttpPost]
        public IActionResult Create(GradeIndexViewModel grade)
        {
            var model = new Grade();
            model.Ativo = true;
            model.Dia = grade.gradeSelected.Dia;
            model.IdFuncionario = grade.gradeSelected.IdFuncionario;
            model.IdCliente = grade.gradeSelected.IdCliente;
            model.Entrada = grade.gradeSelected.Entrada;
            model.Saida = grade.gradeSelected.Saida;
            model.Inicio = DateTime.Now.ToString("dd/MM/yyyy");
            _grade.Add(model);
            return RedirectToAction(nameof(Index));
        }
    }
}