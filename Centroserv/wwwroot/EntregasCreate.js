﻿var material = document.querySelector("#txtMaterial");
var materialID = document.querySelector("#txtMaterialID");
var quantidade = document.querySelector("#txtQuantidade");
var unidade = document.getElementById("txtUnidade");
var valor_unitario = document.querySelector("#txtValorUnitario");
var botaoAdd = document.querySelector("#btnAdd");
var cliente = document.querySelector('#txtCliente');
var custoTotal = document.querySelector('#CustoTotal');

    
function AdicionaItem(e) {
    if (!(material.value && materialID.value)) {
        material.focus();
        return false;
    }
    // Cria Descricao Material
    var node_material = document.createElement("input");
    node_material.value = material.value;
    node_material.setAttribute("id", "itemMaterial");
    node_material.setAttribute("name", "itemMaterial");
    node_material.setAttribute("type", "hidden");

    // Cria Quantidade
    var node_qtd = document.createElement("input");
    node_qtd.value = quantidade.value;
    node_qtd.setAttribute("id", "itemQuantidade");
    node_qtd.setAttribute("name", "itemQuantidade");
    node_qtd.setAttribute("type", "hidden");

    // Cria ID Material
    var node_materialID = document.createElement("input");
    node_materialID.setAttribute("id", "itemID");
    node_materialID.setAttribute("name", "itemID");
    node_materialID.value = materialID.value;
    node_materialID.setAttribute("type", "hidden");

    // Cria Custo Unitario
    var node_valor_unitario = document.createElement("input");
    node_valor_unitario.setAttribute("id", "txtValorUnitario");
    node_valor_unitario.setAttribute("name", "txtValorUnitario");
    node_valor_unitario.value = valor_unitario.value;
    node_valor_unitario.setAttribute("type", "hidden");


    // Cria Button para excluir
    var node_button = document.createElement("i");
    node_button.classList.add("far");
    node_button.classList.add("fa-trash-alt");
    node_button.classList.add("text-danger");
    node_button.setAttribute("id", "btnExcluir");


    // Cria Span com QTD
    var node_span = document.createElement("span");
    node_span.setAttribute("name", "spanQts");
    node_span.classList.add("badge");
    node_span.classList.add("badge-success");
    node_span.classList.add("badge-pill");
    node_span.innerText = quantidade.value + " " + unidade.value;

    var node_lista_item = document.createElement("li")
    node_lista_item.setAttribute("id", "LI_Itens");
    node_lista_item.innerText = material.value + " ";
    node_lista_item.appendChild(node_span);
    node_lista_item.appendChild(document.createTextNode('\u00A0\u00A0')); // Adiciona espaco em branco logo apos do SPAN
    node_lista_item.appendChild(node_button);

    node_lista_item.appendChild(node_materialID);
    node_lista_item.appendChild(node_material);
    node_lista_item.appendChild(node_qtd);
    node_lista_item.appendChild(node_valor_unitario);

    //var TextNode = document.createTextNode(material.value);
    //node.appendChild(TextNode);

    //document.querySelector("#entregas_div").appendChild(node_materialID);
    //document.querySelector("#entregas_div").appendChild(node_material);
    //document.querySelector("#entregas_div").appendChild(node_qtd);
    //document.querySelector("#entregas_div").appendChild(document.createElement("br"));
    document.querySelector("#entregas_lista").appendChild(node_lista_item);

    if (custoTotal != null) {
        var totalval = (custoTotal.value).replace(",", "");
        var valor = parseFloat(totalval) + parseFloat(valor_unitario.value);
        custoTotal.value = parseFloat(Math.round(valor * 100) / 100).toFixed(2)

    }

    // Zera campos apos adicionar material
    botaoAdd.classList.add("d-none");
    document.querySelector("#txtMaterial").value = "";
    document.querySelector("#txtMaterialID").value = "";
    document.querySelector("#txtQuantidade").value = "";
    document.getElementById("txtUnidade").value = "";
    document.querySelector("#txtValorUnitario").value = "";

    document.querySelector("#txtMaterial").focus();

    //Adiciona Event Listener no BTN Exlcuir
    node_button.addEventListener('click', function (e) {
        console.log(e.target.parentNode.remove());
    });

    
}

function autocomplete(inp, arr) {
        /*the autocomplete function takes two arguments,
        the text field element and an array of possible autocompleted values:*/
        var currentFocus;
        /*execute a function when someone writes in the text field:*/
        let vetorMateriais = arr;
    inp.addEventListener("input", function (e) {
        var a, b, i, val = this.value;
        /*close any already open lists of autocompleted values*/
        closeAllLists();
        if (!val) { return false; }
        currentFocus = -1;
        /*create a DIV element that will contain the items (values):*/
        a = document.createElement("DIV");
        a.setAttribute("id", this.id + "autocomplete-list");
        a.setAttribute("class", "autocomplete-items");
        /*append the DIV element as a child of the autocomplete container:*/
        this.parentNode.appendChild(a);
        /*for each item in the array...*/
        for (i = 0; i < arr.length; i++) {
            /*check if the item starts with the same letters as the text field value:*/
            if (arr[i][0].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
                /*create a DIV element for each matching element:*/
                b = document.createElement("DIV");
                /*make the matching letters bold:*/
                b.innerHTML = "<strong>" + arr[i][0].substr(0, val.length) + "</strong>";
                b.innerHTML += arr[i][0].substr(val.length);
                /*insert a input field that will hold the current array item's value (descrição):*/
                b.innerHTML += "<input type='hidden' value='" + arr[i][0] + "'>";
                /*execute a function when someone clicks on the item value (DIV element):*/
                b.addEventListener("click", function (e) {
                    /*insert the value for the autocomplete text field:*/
                    material.value = this.getElementsByTagName("input")[0].value;
                    /* Pesquisa no array de materiais a UNIDADE e QUANTIDADE PADRAO*/
                    vetorMateriais.forEach(function (entry) {
                        if (entry[0] == material.value) {
                            unidade.value = entry[1]
                            quantidade.value = entry[2]; // posição para QUANTIDADE PADRAO
                            materialID.value = entry[3];
                            valor_unitario.value = entry[4];
                            botaoAdd.classList.remove("d-none");
                        }
                    });

                    /*close the list of autocompleted values,
                    (or any other open lists of autocompleted values:*/
                    closeAllLists();
                });
                a.appendChild(b);
            }
        }
    });
/*execute a function presses a key on the keyboard:*/
        inp.addEventListener("keydown", function (e) {
                var x = document.getElementById(this.id + "autocomplete-list");
            if (x) x = x.getElementsByTagName("div");
                if (e.keyCode == 40) {
                /*If the arrow DOWN key is pressed,
                increase the currentFocus variable:*/
                currentFocus++;
            /*and and make the current item more visible:*/
            addActive(x);
                } else if (e.keyCode == 38) { //up
                /*If the arrow UP key is pressed,
                decrease the currentFocus variable:*/
                currentFocus--;
            /*and and make the current item more visible:*/
            addActive(x);
                } else if (e.keyCode == 13) {
                /*If the ENTER key is pressed, prevent the form from being submitted,*/
                e.preventDefault();
            if (currentFocus > -1) {
                        /*and simulate a click on the "active" item:*/
                        if (x) x[currentFocus].click();
        }
    }
});
        function addActive(x) {
                /*a function to classify an item as "active":*/
                if (!x) return false;
            /*start by removing the "active" class on all items:*/
            removeActive(x);
            if (currentFocus >= x.length) currentFocus = 0;
                if (currentFocus < 0) currentFocus = (x.length - 1);
            /*add class "autocomplete-active":*/
            x[currentFocus].classList.add("autocomplete-active");
        }
            function removeActive(x) {
                /*a function to remove the "active" class from all autocomplete items:*/
                for (var i = 0; i < x.length; i++) {
                x[i].classList.remove("autocomplete-active");
            }
        }
            function closeAllLists(elmnt) {
                /*close all autocomplete lists in the document,
                except the one passed as an argument:*/
                var x = document.getElementsByClassName("autocomplete-items");
                for (var i = 0; i < x.length; i++) {
                    if (elmnt != x[i] && elmnt != inp) {
                x[i].parentNode.removeChild(x[i]);
            }
        }
    }
    /*execute a function when someone clicks in the document:*/
            document.addEventListener("click", function (e) {
                closeAllLists(e.target);
            });
    }






    function autocompleteClientes(inp, arr) {
        /*the autocomplete function takes two arguments,
        the text field element and an array of possible autocompleted values:*/
        var currentFocusCliente;
            /*execute a function when someone writes in the text field:*/
            let vetorClientes = arr;
        inp.addEventListener("input", function (e) {
                let a, b, i, val = this.value;
            /*close any already open lists of autocompleted values*/
            closeAllLists();
            if (!val) { return false; }
            currentFocusCliente = -1;
            /*create a DIV element that will contain the items (values):*/
            a = document.createElement("DIV");
            a.setAttribute("id", this.id + "autocomplete-list");
            a.setAttribute("class", "autocomplete-items");
            /*append the DIV element as a child of the autocomplete container:*/
            this.parentNode.appendChild(a);
            /*for each item in the array...*/
            for (i = 0; i < arr.length; i++) {
                /*check if the item starts with the same letters as the text field value:*/
                if (arr[i][0].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
                /*create a DIV element for each matching element:*/
                b = document.createElement("DIV");
            /*make the matching letters bold:*/
            b.innerHTML = "<strong>" + arr[i][0].substr(0, val.length) + "</strong>";
            b.innerHTML += arr[i][0].substr(val.length);
            /*insert a input field that will hold the current array item's value (descrição):*/
                    b.innerHTML += "<input type='hidden' value='" + arr[i][0] + "'>";
            /*execute a function when someone clicks on the item value (DIV element):*/
                    b.addEventListener("click", function (e) {
                    /*insert the value for the autocomplete text field:*/
                    cliente.value = this.getElementsByTagName("input")[0].value;
                /* Pesquisa no array de materiais a UNIDADE e QUANTIDADE PADRAO*/
                vetorClientes.forEach(function (entry) {
                            if (entry[0] == cliente.value) {
                    document.querySelector('#txtClienteID').value = entry[1]
                }
                });

                /*close the list of autocompleted values,
                (or any other open lists of autocompleted values:*/
                closeAllLists();
            });
            a.appendChild(b);
        }
    }
});
/*execute a function presses a key on the keyboard:*/
        inp.addEventListener("keydown", function (e) {
                    let x = document.getElementById(this.id + "autocomplete-list");
                if (x) x = x.getElementsByTagName("div");
            if (e.keyCode == 40) {
                    /*If the arrow DOWN key is pressed,
                    increase the currentFocusCliente variable:*/
                    currentFocusCliente++;
                /*and and make the current item more visible:*/
                addActive(x);
            } else if (e.keyCode == 38) { //up
                    /*If the arrow UP key is pressed,
                    decrease the currentFocusCliente variable:*/
                    currentFocusCliente--;
                /*and and make the current item more visible:*/
                addActive(x);
            } else if (e.keyCode == 13) {
                    /*If the ENTER key is pressed, prevent the form from being submitted,*/
                    e.preventDefault();
                if (currentFocusCliente > -1) {
                    /*and simulate a click on the "active" item:*/
                    if (x) x[currentFocusCliente].click();
            }
        }
    });
        function addActive(x) {
            /*a function to classify an item as "active":*/
            if (!x) return false;
                /*start by removing the "active" class on all items:*/
                removeActive(x);
                if (currentFocusCliente >= x.length) currentFocusCliente = 0;
            if (currentFocusCliente < 0) currentFocusCliente = (x.length - 1);
                /*add class "autocomplete-active":*/
                x[currentFocusCliente].classList.add("autocomplete-active");
            }
        function removeActive(x) {
            /*a function to remove the "active" class from all autocomplete items:*/
            for (var i = 0; i < x.length; i++) {
                    x[i].classList.remove("autocomplete-active");
                }
            }
        function closeAllLists(elmnt) {
                    /*close all autocomplete lists in the document,
                    except the one passed as an argument:*/
                    let x = document.getElementsByClassName("autocomplete-items");
            for (var i = 0; i < x.length; i++) {
                if (elmnt != x[i] && elmnt != inp) {
                    x[i].parentNode.removeChild(x[i]);
                }
            }
        }
        /*execute a function when someone clicks in the document:*/
        document.addEventListener("click", function (e) {
                    closeAllLists(e.target);
                });
            }
        
        
if (cliente != null) {
    autocompleteClientes(cliente, ClientesVetor);
}        

autocomplete(material, MateriaisVetor);
        
