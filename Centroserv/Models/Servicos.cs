﻿using Centroserv.Models.ValoresPadroes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Centroserv.Models
{
    public class Servicos
    {
        [Key]
        public int IdServico { get; set; }
        public int IdCliente { get; set; }
        [Display(Name="Data")]
        public DateTime DataServico { get; set; }
        [Display(Name = "Hora")]
        public TimeSpan HoraServico { get; set; }
        [Display(Name = "Observacao")]
        public string Obs { get; set; }
        [Display(Name ="Servico")]
        public TiposServico TipoServico { get; set; }
        [Display(Name = "Realizado")]
        public bool Realizado { get; set; }
        public string Usuario { get; set; }


        public Clientes Cliente { get; set; }


    }
}
