﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Centroserv.Models
{
    public class EntregaItens
    {
        public int IdEntrega { get; set; }
        public int IdItem { get; set; }

        public decimal Quantidade { get; set; }
        public decimal CustoFinal { get; set; }
        public DateTime DataEntrega { get; set; }

        public Material Material { get; set; }
        public Entregas Entrega { get; set; }

    }
}
