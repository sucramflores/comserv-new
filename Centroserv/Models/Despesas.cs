﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Centroserv.Models.ValoresPadroes;

namespace Centroserv.Models
{
    public class Despesas
    {
        public int Id { get; set; }
        public int IdCliente { get; set; }
        public string Descricao { get; set; }
        public TipoDespesa TipoDespesa { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public double Valor { get; set; }
        public string Data { get; set; }

        public Clientes Cliente { get; set; }

    }


}
