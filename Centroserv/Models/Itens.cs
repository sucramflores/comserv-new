﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Centroserv.Models
{
    public class Itens
    {
        public int IdItem { get; set; }
        public string Descricao { get; set; }
        public string Unidade { get; set; }
        public double QtdPadrao { get; set; }
        public double CustoUnitario { get; set; }
        public DateTime UltimaAtualizacao { get; set; }
    }
}
