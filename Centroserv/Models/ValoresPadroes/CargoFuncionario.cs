﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Centroserv.Models.ValoresPadroes
{
    public enum CargoFuncionario
    {
        Limpeza,
        Zelador,
        Porteiro,
        Supervisor,
        Estagiario,
        Administrativo

    }
}
