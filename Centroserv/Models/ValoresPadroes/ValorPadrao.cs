﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Centroserv.Models.ValoresPadroes
{
    public enum TipoDespesa
    {
        Operacional,
        Rembolso,
        Outros
    }

    //public enum TipoDespesas
    //{
    //    Material_Limpeza,
    //    Hora_Extra,
    //    Outros
    //}
}
