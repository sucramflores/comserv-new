﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Centroserv.Models
{
    public class Entregas
    {
        public int IdEntrega { get; set; }
        public int IdCliente { get; set; }
        public bool Entregue { get; set; }

        [DataType(DataType.Date)]
        public DateTime Solicitado { get; set; }
        
        //[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        [Display(Name = "Previsao Entrega")]
        [DataType(DataType.Date)]
        public DateTime DataEntrega { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        [Display(Name = "Custo")]
        public decimal CustoTotal { get; set; }
        public String Obs { get; set; }
        public string Responsavel { get; set; }

        public Clientes Cliente { get; set; }
        public List<EntregaItens> Itens { get; set; }
    }



   
}
