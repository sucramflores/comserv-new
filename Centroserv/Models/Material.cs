﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Centroserv.Models.ValoresPadroes;

namespace Centroserv.Models
{
    public class Material
    {
        public int Id { get; set; }
        public string Descricao { get; set; }

        [Display(Name = "Unidade de Fornecimento")]
        public string Unidade { get; set; }

        [Display(Name = "Quantidade Padrao")]
        public double QtdPadrao { get; set; }

        [Display(Name = "Valor Unitario")]
        public double ValorUnitario { get; set; }

        [Display(Name = "Data Ultimo Reajuste")]
        public string DataUltimoReajuste { get; set; }

    }
}
