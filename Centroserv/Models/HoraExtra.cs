﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Centroserv.Models
{
    public class HoraExtra
    {
        public int Id { get; set; }
        public int IdFuncionario { get; set; }
        public int IdCliente { get; set; }
        public string Data { get; set; }
        public decimal TotalHoras { get; set; }
        public string ObsHoraExtra { get; set; }

        public Clientes Cliente { get; set; }
    }
}
