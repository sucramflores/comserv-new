﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Centroserv.Models.ValoresPadroes;

namespace Centroserv.Models
{
    public class Grade
    {
        public int Id { get; set; }
        public int IdFuncionario { get; set; }
        public int IdCliente { get; set; }
        public bool Ativo { get; set; }
        public string Entrada { get; set; }
        public string Saida { get; set; }

        public string Inicio { get; set; }

        public string Fim { get; set; }

        public DiasGrade Dia { get; set; }

        public Funcionarios Funcionarios { get; set; }

        public Clientes Clientes { get; set; }

    }
}
