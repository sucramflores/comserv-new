﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Centroserv.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Rewrite;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;

namespace Centroserv
{
    public class Startup
    {
        private IConfiguration _configuration;
        private readonly string TenantName = "centroserv2.onmicrosoft.com"; // aka 'Initial Domain Name' in Azure  
        private readonly string ClientId = "6a83023-43fb-41db-b795-657011f0b1df"; // aka 'Application Id' in Azure  


        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;

        }
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddAuthentication(options =>
            {
                options.DefaultScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                options.DefaultSignInScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = OpenIdConnectDefaults.AuthenticationScheme; 
            })
            .AddCookie()
            .AddOpenIdConnect(options =>
            {
                options.Authority = "https://login.microsoftonline.com/" +this.TenantName;
                options.ClientId = this.ClientId;
                options.ResponseType = OpenIdConnectResponseType.IdToken;
                options.CallbackPath = "/signin-oidc";
                //options.CallbackPath = "/security/signin-callback";
                options.SignedOutRedirectUri = "https://localhost:44335/";
            });

            services.AddSingleton<IClientes, DBClientes>();
            services.AddSingleton<IImobiliarias, InMemoryImobiliairas>();
            services.AddSingleton<IFuncionarios, InMemoryFuncionarios>();
            services.AddSingleton<IFaltas, InMemoryFaltas>();
            services.AddSingleton<IHoraExtra, InMemoryHoraExtra>();
            services.AddSingleton<IMaterial, InMemoryMaterial>();
            services.AddSingleton<IDespesas, InMemoryDespesa>();
            services.AddSingleton<IGrade, InMemoryGrade>();
            services.AddSingleton<IEntregas, InMemoryEntregas>();
            services.AddSingleton<IServicos, InMemoryServicos>();

            services.AddMvc(options => {
                //options.Filters.Add(new AutoValidateAntiforgeryTokenAttribute());
                options.Filters.Add(new RequireHttpsAttribute());
            })
            .AddRazorOptions(options => options.PageViewLocationFormats.Add("/Views/Shared/AutoComplete/{0}.cshtml"));


        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, 
                              IHostingEnvironment env,
                              IClientes clientes,
                              IImobiliarias imobiliarias,
                              IFuncionarios funcionarios,
                              IGrade grade,
                              IFaltas faltas,
                              IMaterial material,
                              IDespesas despesas,
                              IHoraExtra horaex,
                              IEntregas entrega)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseStaticFiles();

            //app.UseRewriter(new RewriteOptions()
            //                    .AddRedirectToHttpsPermanent());

            app.UseAuthentication();

            app.UseMvc(ConfigureRoutes);

            app.Run(async (context) =>
            {
                await context.Response.WriteAsync("Em construcao!!");
            });
        }

        private void ConfigureRoutes(IRouteBuilder routeBuilder)
        {
            // /Home/Index/4

            routeBuilder.MapRoute(
                    name : "Default",
                    template: "{controller=Entregas}/{action=Index}/{id?}");

            
        }
    }
}
