﻿using Centroserv.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Centroserv.ViewModels
{
    public class ClientesEditViewModel
    {
        //public int Id { get; set; }
        public TipoEstabelecimento Tipo { get; set; }

        [Required, MaxLength(50)]
        public string Alias { get; set; }
        public string Razao { get; set; }
        public string Endereco { get; set; }
        public string Bairro { get; set; }
        public string Cidade { get; set; }
        public string UF { get; set; }

        public Imobiliarias Imobiliaria { get; set; }
        public List<Imobiliarias> ListaImobiliarias { get; set; }
        public int SelectedImobiliaria { get; set; }

        public TipoContato Contato { get; set; }
        public string NomeContato { get; set; }
        public string EnderecoContato { get; set; }
        public string EmailContato { get; set; }
        public string FoneContato { get; set; }
        public string CelularContato { get; set; }

        public string Inicio { get; set; }
        public string Fim { get; set; }
        public bool MaterialLimpeza { get; set; }
        public string Observacao { get; set; }
        public bool SerZeladoria { get; set; }
        public bool SerLimpeza { get; set; }
        public bool SerPortaria { get; set; }
        public bool SerGramado { get; set; }

        public string Equipamentos { get; set; }
        public double ValorMaximoMaterial { get; set; }
        //[BindProperty]
        //public Clientes Clientes { get; set; }

        public double Latitude { get; set; }
        public double Longitude { get; set; }



    }
}
