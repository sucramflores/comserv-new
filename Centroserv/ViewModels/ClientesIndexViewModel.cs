﻿using Centroserv.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Centroserv.ViewModels
{
    public class ClientesIndexViewModel
    {
        public IEnumerable<Clientes> Clientes { get; set; }

    }
}
