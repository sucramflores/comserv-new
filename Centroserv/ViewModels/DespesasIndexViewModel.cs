﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Centroserv.Models;
using Centroserv.Models.ValoresPadroes;

namespace Centroserv.ViewModels
{
    public class DespesasIndexViewModel
    {
        public IEnumerable<Despesas> Despesas { get; set; }
        public IEnumerable<Clientes> ListaClientes { get; set; }

        //Used to create a new despesa
        public int Id { get; set; }
        public int IdCliente { get; set; }
        public string Descricao { get; set; }
        [Display(Name = "Tipo de Despesa")]
        public TipoDespesa TipoDespesa { get; set; }
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public double Valor { get; set; }
        public string Data { get; set; }
        public Clientes Cliente { get; set; }

    }
}
