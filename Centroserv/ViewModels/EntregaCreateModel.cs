﻿using Centroserv.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Centroserv.ViewModels
{
    public class EntregaCreateModel
    {
        public string txtCliente { get; set; }
        public int txtClienteID { get; set; }
        public string txtObservacao { get; set; }
        public string txtResponsavel { get; set; }


        public string[] itemMaterial { get; set; }
        public decimal[] itemQuantidade { get; set; }
        public int[] itemID { get; set; }

        public DateTime dataSolicitacao { get; set; }
        public DateTime dataEntrega { get; set; }

    }
}
