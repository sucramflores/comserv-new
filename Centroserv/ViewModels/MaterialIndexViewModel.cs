﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Centroserv.Models;

namespace Centroserv.ViewModels
{
    public class MaterialIndexViewModel
    {
        public IEnumerable<Material> Materiais { get; set; }
    }
}
