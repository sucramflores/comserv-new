﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Centroserv.Models.ValoresPadroes;
using Centroserv.Models;

namespace Centroserv.ViewModels
{
    public class FuncionariosEditViewModel
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Alias { get; set; }
        public string Endereco { get; set; }
        public string Bairro { get; set; }
        public string Cidade { get; set; }
        public string UF { get; set; }
        public string Fone { get; set; }
        public string Celular { get; set; }
        [Display(Name = "Indicado por")]
        public string IndicadaPor { get; set; }
        public string Admissao { get; set; }
        public string Saida { get; set; }
        public string Nascimento { get; set; }
        public string Obs { get; set; }
        public string Foto { get; set; }

        [Display(Name="Contato de Emergencia")]
        public string ContatoEmergencia { get; set; }
        [Display(Name = "Fone Contato de Emergencia")]
        public string ContatoEmergenciaCelular { get; set; }
        public StatusFuncionario Status { get; set; }
        public CargoFuncionario Cargo { get; set; }

        public int Segunda { get; set; }
        public int Terca { get; set; }
        public int Quarta { get; set; }
        public int Quinta { get; set; }
        public int Sexta { get; set; }
        public int Sabado { get; set; }
        public int Domingo { get; set; }

        public int IdFuncionario { get; set; }
        public string Data { get; set; }
        public string ObsFalta { get; set; }
        public IEnumerable<Faltas> Faltas { get; set; }

        public int IdCliente { get; set; }
        public int TotalHoras { get; set; }
        public string ObsHoraExtra { get; set; }
        public IEnumerable<Clientes> ClientesTodos { get; set; }
        public IEnumerable<HoraExtra> HorasExtras { get; set; }



    }
}
