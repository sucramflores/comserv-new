﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Centroserv.Models;

namespace Centroserv.ViewModels
{
    public class ImobiliariasEditViewIndex
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Contato { get; set; }
        public string Endereco { get; set; }
        public string Bairro { get; set; }
        public string Cidade { get; set; }
        public string UF { get; set; }
        public string Fone { get; set; }
        public string Email { get; set; }
        public string Obs { get; set; }
        public IEnumerable<Clientes> Clientes { get; set; }
    }
}
