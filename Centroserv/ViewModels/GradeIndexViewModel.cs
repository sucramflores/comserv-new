﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Centroserv.Models;
using Centroserv.Models.ValoresPadroes;

namespace Centroserv.ViewModels
{
    public class GradeIndexViewModel
    {
        public IEnumerable<Grade> GradeDeHorario { get; set; }
        public dynamic GradeClientesUnico { get; set; }

        public IEnumerable<Funcionarios> FuncionariosTodos { get; set; } // usado no dropdown para incluuir novos horarios
        public IEnumerable<Clientes> ClientesTodos { get; set; }

        public Grade gradeSelected { get; set; }
        //public DiasGrade Dia { get; set; }
        //public int SelectedFuncionario { get; set; }
        //public int SelectedCliente { get; set; }
        //public string Entrada { get; set; }
        //public string Saida { get; set; }
        //public DateTime Inicio { get; set; }
    }

  

}
