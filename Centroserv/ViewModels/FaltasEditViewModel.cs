﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Centroserv.ViewModels
{
    public class FaltasEditViewModel
    {
        public int Id { get; set; }
        public int IdFuncionario { get; set; }
        public string Data { get; set; }
        public string ObsFalta { get; set; }
    }
}
