﻿using Centroserv.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Centroserv.ViewModels
{
    public class AgendaIndexViewModel
    {
        public IEnumerable<Entregas> Entregas { get; set; }
        public IEnumerable<Servicos> Servicos { get; set; }
        public Servicos Servico { get; set; } // Handles de model (servico) on Bootsrap modal
        public IEnumerable<Material> MaterialAll { get; set; }
        public IEnumerable<Clientes> Clientes { get; set; }

    }
}
